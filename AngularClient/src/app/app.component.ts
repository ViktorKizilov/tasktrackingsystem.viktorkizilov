import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'AngularClient';
  public email: string;
  isAuth = false;

  constructor(private authService: AuthenticationService, private router: Router) {
    this.authService.currentUser.subscribe(user => {
      if (user) {
        this.isAuth = true;
        this.email = user.email;
      }
      else {
        this.isAuth = false;
        this.email = '';
      }
    })

  }
  ngOnInit() {
    let currentUser = this.authService.getCurrentUserValue();
    this.isAuth = currentUser == null ? false : true;
    this.email = currentUser == null ? '' : currentUser.email;
  }
  logout() {

    this.authService.logout();
    this.router.navigate(['/Authenticate']);
  }

}
