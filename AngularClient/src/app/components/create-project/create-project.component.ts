import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/interfaces/project'
import { ProjectService } from 'src/app/services/project.service';


@Component({
  selector: 'app-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.css']
})
export class CreateProjectComponent implements OnInit {

  constructor(private projectService: ProjectService) { }

  project: Project;

  createProject(project: Project) {
    this.projectService.createProject(project);
  }

  ngOnInit() {
  }

}
