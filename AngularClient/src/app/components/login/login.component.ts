import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';

  constructor(private authService: AuthenticationService, private router: Router) { }

  get f() { return this.loginForm.controls; }

  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl(''),
      password: new FormControl('')
    });
  }

  login() {
    this.authService.login(this.loginForm.value).subscribe(
      (user) => {

        if (user.role === "Admin") {
          this.router.navigate(['/Positions']);
        }
        if (user.role == "User") {
          this.router.navigate(['/Projects']);
        }
      },
      error => {
        this.error = error;
        this.loading = false;
      });
  }
}
