import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/interfaces/project'
import { ProjectFilter } from 'src/app/interfaces/project-filter'
import { ProjectService } from 'src/app/services/project.service';


@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {

  constructor(private projectService: ProjectService) { }

  projects: Project[] = [];
  filter: ProjectFilter = { creatorId: null, contributorsIds: null, creationDate: null, deadLine: null, name: null, projectStatusId: null };
  projectId: number;
  project: Project;
  loadComponent : boolean;

  getProjects(): void {
    this.projectService.getProjects(this.filter).subscribe((data) => {
      this.projects = data;
    });
  }

  getProjectById(): void {
    this.projectService.getProjectById(this.projectId).subscribe(
      (data) => {
        console.log(data);
        this.project = <Project>data;
      });
  }

  openDialog(): void {
    this.loadComponent = true;
  }



  ngOnInit() {
    this.getProjects();
  }

}
