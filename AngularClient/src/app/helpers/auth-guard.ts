import { AuthenticationService } from 'src/app/services/authentication.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
@Injectable({providedIn: 'root'})
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthenticationService, private router: Router){

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
    let currentUser  = this.authService.getCurrentUserValue();
    if(currentUser){
      return route.data.roles.includes(currentUser.role);
    }

    this.router.navigate(['/Authenticate']);
    return false;

  }
}
