export interface ProjectFilter {
    name: string|null;
    creationDate: Date | null;
    deadLine: Date | null;
    projectStatusId: number | null;
    creatorId: number | null;
    contributorsIds: number[] | null;
}
