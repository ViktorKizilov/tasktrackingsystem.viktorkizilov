export interface Project {
    id: number;
    name: string;
    description: string;
    creationDate: Date;
    deadLine: string;
    statusChangedDate: Date;
    projectStatusId: number;
    creatorId: number;
    attachmentsIds: number[];
    contributorsIds: number[];
    assignmentsIds: number[];
}
