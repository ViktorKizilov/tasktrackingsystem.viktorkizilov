export interface UserLogged {
    userId: number;
    email: string;
    role: string;
    token: string;
}