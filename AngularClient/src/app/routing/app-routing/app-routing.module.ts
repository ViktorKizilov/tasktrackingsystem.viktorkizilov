import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from 'src/app/components/login/login.component';
import { ProjectComponent } from 'src/app/components/project/project.component';
import { PositionComponent } from 'src/app/components/position/position.component';
import { AuthGuard } from 'src/app/helpers/auth-guard';


const routes: Routes = [
  { path: 'Authenticate', component: LoginComponent },
  {
    path: 'Projects', component: ProjectComponent, canActivate: [AuthGuard], data: {
      roles: ['Admin', 'User']
    }
  },
  {
    path: 'Positions', component: PositionComponent, canActivate: [AuthGuard],
    data: {
      roles: ['Admin']
    }
  },
  { path: '', redirectTo: '/Projects', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
