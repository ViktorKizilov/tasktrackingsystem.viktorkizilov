import { Injectable } from '@angular/core';
import { UserLogged } from './../interfaces/user-logged';
import { HttpClient } from '@angular/common/http';
import { map } from "rxjs/operators";
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private currentUserSubject: BehaviorSubject<UserLogged>;
  public currentUser: Observable<UserLogged>;
  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<UserLogged>(JSON.parse(localStorage.getItem('currentUser')!));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public getCurrentUserValue(): UserLogged {
    return this.currentUserSubject.value;
  }

  login(userLogin: any) {
    return this.http.post('https://localhost:44336/api/Authentication/', userLogin).pipe(
      map(data => {
        let user = <UserLogged>data;
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(user);
        return user;
      }
      ));
  }

  logout() {
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null!);
  }
}
