import { Injectable } from '@angular/core';
import { Project } from './../interfaces/project'
import { ProjectFilter } from './../interfaces/project-filter'
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  projectsUrl = 'https://localhost:44336/api/Projects';

  constructor(private http: HttpClient) { }

  getProjects(filterProject: ProjectFilter): Observable<Project[]> {
    let params = new HttpParams();

    if (filterProject.creatorId != null)
      params = params.append('CreatorId', `${filterProject.creatorId}`);

    if (filterProject.name != null)
      params = params.append('Name', `${filterProject.name}`);

    if (filterProject.projectStatusId != null)
      params = params.append('ProjectStatusId', `${filterProject.projectStatusId}`);

    return this.http.get<Project[]>(this.projectsUrl, { params })
  }

  getProjectById(projectId: number) {
    return this.http.get(this.projectsUrl + `/${projectId}`);
  }

  createProject(projectModel: Project) {
    return this.http.post(this.projectsUrl, projectModel);
  }

  removeProject(projectId: number) {
    return this.http.get(this.projectsUrl + `/${projectId}`);
  }

  editProject(projectModel: Project) {
    return this.http.put(this.projectsUrl, projectModel);
  }
}
