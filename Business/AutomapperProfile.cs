﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskTrackingSystem.Data.Entities;
using TaskTrackingSystem.Business.Models;

namespace TaskTrackingSystem.Business
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<Assignment, AssignmentModel>()
                .ForMember(am => am.AttachmentsIds, a => a.MapFrom(a => a.Attachments.Select(att => att.Id)))
                .ForMember(am => am.CommitsIds, a => a.MapFrom(a => a.Commits.Select(c => c.Id)))
                .ReverseMap();

            CreateMap<Commit, CommitModel>()
                .ForMember(cm => cm.AttachmentsIds, c => c.MapFrom(c => c.Attachments.Select(att => att.Id)))
                .ReverseMap();

            CreateMap<Department, DepartmentModel>()
                .ForMember(dm => dm.PositionsIds, d => d.MapFrom(d => d.Positions.Select(p => p.Id)))
                .ReverseMap();

            CreateMap<Position, PositionModel>()
                .ForMember(pm => pm.PositionRecordsIds, p => p.MapFrom(p => p.PositionRecords.Select(pr => pr.Id)))
                .ReverseMap();

            CreateMap<Project, ProjectModel>()
                .ForMember(pm => pm.AttachmentsIds, p => p.MapFrom(p => p.Attachments.Select(att => att.Id)))
                .ForMember(pm => pm.ContributorsIds, p => p.MapFrom(p => p.Contributors.Select(c => c.Id)))
                .ForMember(pm => pm.AssignmentsIds, p => p.MapFrom(p => p.Assignments.Select(a => a.Id)))
                .ReverseMap();

            CreateMap<User, UserModel>()
                .ForMember(um => um.PositionsHistoryIds, u => u.MapFrom(u => u.PositionsHistory.Select(ph => ph.Id)))
                .ForMember(um => um.CreatedProjectsIds, u => u.MapFrom(u => u.CreatedProjects.Select(cp => cp.Id)))
                .ForMember(um => um.ContributedProjectsIds, u => u.MapFrom(u => u.ContributedProjects.Select(cp => cp.Id)))
                .ForMember(um => um.CreatedAssignmentsIds, u => u.MapFrom(u => u.CreatedAssignments.Select(ca => ca.Id)))
                .ForMember(um => um.ContributedAssignmentsIds, u => u.MapFrom(u => u.ContributedAssignments.Select(a => a.Id)))
                .ReverseMap();

            CreateMap<PositionRecord, PositionRecordModel>().ReverseMap();
        }
    }
}
