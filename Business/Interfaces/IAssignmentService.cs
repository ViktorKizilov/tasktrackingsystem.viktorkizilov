﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TaskTrackingSystem.Business.Models;

namespace TaskTrackingSystem.Business.Interfaces
{
    public interface IAssignmentService : ICrud<AssignmentModel>, IAttachmentService
    {
        IEnumerable<AssignmentModel> GetByFilter(AssignmentFilterModel filterModel);
    }
}
