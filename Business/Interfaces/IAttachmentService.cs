﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TaskTrackingSystem.Data.Entities;

namespace TaskTrackingSystem.Business.Interfaces
{
    public interface IAttachmentService
    {
        Task<Attachment> GetAttachmentById(int id);
        Task AddAttachmentsAsync(IFormFileCollection attachments, int containerId, string path);
        Task DeleteAttachmentAsync(int attachmentId);
        IEnumerable<Attachment> GetAttachmentsByContainerId(int id);
    }
}
