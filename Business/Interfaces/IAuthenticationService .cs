﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TaskTrackingSystem.Business.Models;

namespace TaskTrackingSystem.Business.Interfaces
{
    public interface IAuthenticationService
    {
        UserLoggedModel Authenticate(UserAuthModel userAuthModel);
    }
}
