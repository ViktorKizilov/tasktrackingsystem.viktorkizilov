﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TaskTrackingSystem.Business.Models;

namespace TaskTrackingSystem.Business.Interfaces
{
    public interface ICommitService : ICrud<CommitModel>, IAttachmentService
    {
        IEnumerable<CommitModel> GetByFilter(CommitFilterModel filterModel);
    }
}
