﻿using System;
using System.Collections.Generic;
using System.Text;
using TaskTrackingSystem.Business.Models;

namespace TaskTrackingSystem.Business.Interfaces
{
    public interface IPositionService : ICrud<PositionModel>
    {
    }
}
