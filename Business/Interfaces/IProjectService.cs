﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TaskTrackingSystem.Business.Models;
using TaskTrackingSystem.Data.Entities;

namespace TaskTrackingSystem.Business.Interfaces
{
    public interface IProjectService : ICrud<ProjectModel>, IAttachmentService
    {
        IEnumerable<ProjectModel> GetByFilter(ProjectFilterModel filterModel);
    }
}
