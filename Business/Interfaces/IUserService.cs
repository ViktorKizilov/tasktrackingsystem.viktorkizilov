﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TaskTrackingSystem.Business.Models;

namespace TaskTrackingSystem.Business.Interfaces
{
    public interface IUserService : ICrud<UserModel>
    {
        Task ChangeUserPositionAsync(PositionRecordModel newPositioRecordModel);
        IEnumerable<PositionRecordModel> GetUserPositionsHistory(int userId);
    }
}
