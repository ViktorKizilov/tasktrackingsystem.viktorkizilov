﻿using System;

namespace TaskTrackingSystem.Business.Models
{
    public class AssignmentFilterModel
    {
        public int? ProjectId { get; set; }
        public string Name { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? DeadLine { get; set; }
        public int? StatusId { get; set; }
        public int? CreatorId { get; set; }
        public int? ExecutorId { get; set; }
    }
}