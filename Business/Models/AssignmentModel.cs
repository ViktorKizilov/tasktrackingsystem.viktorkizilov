﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TaskTrackingSystem.Business.Models
{
    public class AssignmentModel
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        public DateTime CreationDate { get; set; }

        [Required]
        public DateTime DeadLine { get; set; }

        public DateTime StatusChangedDate { get; set; }
        public int StatusId { get; set; }
        public int CreatorId { get; set; }
        public int ExecutorId { get; set; }
        public ICollection<int> AttachmentsIds { get; set; }
        public ICollection<int> CommitsIds { get; set; }
    }
}
