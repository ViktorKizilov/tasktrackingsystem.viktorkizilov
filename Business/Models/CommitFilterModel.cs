﻿using System;

namespace TaskTrackingSystem.Business.Models
{
    public class CommitFilterModel
    {
        public int? AssignmentId { get; set; }
        public string Name { get; set; }
        public int? ContributorId { get; set; }
        public DateTime? CreationDate { get; set; }
    }
}