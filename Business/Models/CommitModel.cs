﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskTrackingSystem.Business.Models
{
    public class CommitModel
    {
        public int Id { get; set; }
        public int AssignmentId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int ContributorId { get; set; }
        public DateTime CreationDate { get; set; }
        public ICollection<int> AttachmentsIds { get; set; }
    }
}
