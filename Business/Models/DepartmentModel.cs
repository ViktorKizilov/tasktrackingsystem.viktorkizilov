﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TaskTrackingSystem.Business.Models
{
    public class DepartmentModel
    {
        public int Id { get; set; }
        
        [Required]
        public string Name { get; set; }
        public int? BossDepartmentId { get; set; }
        public ICollection<int> PositionsIds { get; set; }
    }
}
