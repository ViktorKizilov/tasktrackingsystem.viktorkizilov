﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TaskTrackingSystem.Business.Models
{
    public class PositionModel
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
        public int? BossPositionId { get; set; }

        [Required]
        public int DepartmentId { get; set; }
        public ICollection<int> PositionRecordsIds { get; set; }
    }
}
