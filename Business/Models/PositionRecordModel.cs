﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TaskTrackingSystem.Data;

namespace TaskTrackingSystem.Business.Models
{
    public class PositionRecordModel
    {
        public int Id { get; set; }

        [Required]
        public int UserId { get; set; }

        [Required]
        public int PositionId { get; set; }

        [Required]
        public DateTime FirstDay { get; set; }
        public DateTime? LastDay { get; set; }
    }
}
