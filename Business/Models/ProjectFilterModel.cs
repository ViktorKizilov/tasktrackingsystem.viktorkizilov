﻿using System;
using System.Collections.Generic;

namespace TaskTrackingSystem.Business.Models
{
    public class ProjectFilterModel
    {
        public string Name { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? DeadLine { get; set; }
        public int? ProjectStatusId { get; set; }
        public int? CreatorId { get; set; }
        public ICollection<int> ContributorsIds { get; set; }
    }
}