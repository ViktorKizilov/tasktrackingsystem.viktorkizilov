﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TaskTrackingSystem.Business.Models
{
    public class ProjectModel
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }

        [Required]
        public DateTime DeadLine { get; set; }
        public DateTime StatusChangedDate { get; set; }
        public int ProjectStatusId { get; set; }
        public int CreatorId { get; set; }
        public ICollection<int> AttachmentsIds { get; set; }
        public ICollection<int> ContributorsIds { get; set; }
        public ICollection<int> AssignmentsIds { get; set; }
    }
}
