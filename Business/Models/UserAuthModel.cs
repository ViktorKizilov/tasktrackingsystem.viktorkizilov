﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskTrackingSystem.Business.Models
{
    public class UserAuthModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
