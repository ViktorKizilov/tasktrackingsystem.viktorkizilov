﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskTrackingSystem.Business.Models
{
    public class UserLoggedModel
    {
        public int UserId { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public string Token { get; set; }
    }
}
