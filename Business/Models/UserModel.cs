﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.RegularExpressions;
using TaskTrackingSystem.Business.Validation;

namespace TaskTrackingSystem.Business.Models
{
    public class UserModel : IValidatableObject
    {
        public int Id { get; set; }
        public int? NewPositionId { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime BirthDate { get; set; }

        [Required]
        public int RoleId { get; set; }

        public ICollection<int> PositionsHistoryIds { get; set; }
        public ICollection<int> CreatedProjectsIds { get; set; }
        public ICollection<int> ContributedProjectsIds { get; set; }
        public ICollection<int> CreatedAssignmentsIds { get; set; }
        public ICollection<int> ContributedAssignmentsIds { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (string.IsNullOrWhiteSpace(this.Name) || !Regex.IsMatch(this.Name, @"^[a-zA-Z]+$") || this.Name.Length < 2)
            {
                errors.Add(new ValidationResult("Name can't be empty, contains any other symbols but letters and be less than 2 letters", new List<string>() { "Name" }));
            }
            if (string.IsNullOrWhiteSpace(this.Surname) || !Regex.IsMatch(this.Surname, @"^[a-zA-Z]+$") || this.Surname.Length < 2)
            {
                errors.Add(new ValidationResult("Surname can't be empty, contains any other symbols but letters and be less than 2 letters", new List<string>() { "Surname" }));
            }
            if (!Regex.IsMatch(this.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
            {
                errors.Add(new ValidationResult("Input valid e-mail", new List<string>() { "Email" }));
            }
            if (string.IsNullOrWhiteSpace(this.Password) || !Regex.IsMatch(this.Password, @"(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{4,}$"))
            {
                errors.Add(new ValidationResult("Password should contain at least 4 symbols, among which should be at least one: Capital and small letter, number and special symbol", new List<string>() { "Password" }));
            }
            if (this.BirthDate >= DateTime.Now)
            {
                errors.Add(new ValidationResult("Date of birth can't be in the future", new List<string>() { "BirthDate" }));
            }
            return errors;
        }
    }
}
