﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTrackingSystem.Data.Entities;
using TaskTrackingSystem.Data.Interfaces;
using TaskTrackingSystem.Business.Interfaces;
using TaskTrackingSystem.Business.Models;
using TaskTrackingSystem.Business.Validation;
using System.IO;

namespace TaskTrackingSystem.Business.Services
{
    public class AssignmentService : IAssignmentService
    {
        private IUnitOfWork taskSystemDb { get; set; }
        private Mapper mapper { get; set; }
        public AssignmentService(IUnitOfWork unitOfWork, Mapper mapper)
        {
            taskSystemDb = unitOfWork;
            this.mapper = mapper;
        }

        public async Task AddAsync(AssignmentModel model)
        {
            if (model.DeadLine < model.CreationDate)
                throw new IncorrectInputException("Deadline can't be in the past");

            await taskSystemDb.AssignmentRepository.AddAsync(mapper.Map<AssignmentModel, Assignment>(model));
            await taskSystemDb.SaveAsync();
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            await Task.Run(() => taskSystemDb.AssignmentRepository.DeleteByIdAsync(modelId));
            await taskSystemDb.SaveAsync();
        }

        public IEnumerable<AssignmentModel> GetAll()
        {
            return mapper.Map<IEnumerable<Assignment>, IEnumerable<AssignmentModel>>(taskSystemDb.AssignmentRepository.GetAllWithDetails().AsEnumerable());
        }

        public IEnumerable<AssignmentModel> GetByFilter(AssignmentFilterModel filterModel)
        {
            IQueryable<Assignment> assignments = taskSystemDb.AssignmentRepository.GetAllWithDetails();

            if (filterModel.ProjectId != null)
                assignments = assignments.Where(p => p.ProjectId == filterModel.ProjectId);

            if (filterModel.Name != null)
                assignments = assignments.Where(p => p.Name.ToLower().Contains(filterModel.Name.ToLower()));

            if (filterModel.CreationDate != null)
                assignments = assignments.Where(p => p.CreationDate == filterModel.CreationDate);

            if (filterModel.DeadLine != null)
                assignments = assignments.Where(p => p.DeadLine == filterModel.DeadLine);

            if (filterModel.StatusId != null)
                assignments = assignments.Where(p => p.StatusId == filterModel.StatusId);

            if (filterModel.CreatorId != null)
                assignments = assignments.Where(p => p.CreatorId == filterModel.CreatorId);

            if (filterModel.ExecutorId != null)
                assignments = assignments.Where(p => p.ExecutorId == filterModel.ExecutorId);

            return mapper.Map<IEnumerable<Assignment>, IEnumerable<AssignmentModel>>(assignments.AsEnumerable());
        }

        public async Task<AssignmentModel> GetByIdAsync(int id)
        {
            return await Task.Run(() => mapper.Map<Assignment, AssignmentModel>(taskSystemDb.AssignmentRepository.GetByIdWithDetailsAsync(id).Result));
        }

        public async Task UpdateAsync(AssignmentModel model)
        {
            if (model.DeadLine < model.CreationDate)
                throw new IncorrectInputException("Deadline can't be in the past");

            taskSystemDb.AssignmentRepository.Update(mapper.Map<AssignmentModel, Assignment>(model));
            await taskSystemDb.SaveAsync();
        }

        public async Task<Attachment> GetAttachmentById(int id)
        {
            return await Task.Run(() => taskSystemDb.AttachmentRepository.GetByIdAsync(id));
        }

        public async Task AddAttachmentsAsync(IFormFileCollection attachments, int assignmentId, string path)
        {
            foreach (var attachment in attachments)
            {
                int copyNumber = 0;
                string initPath = path + attachment.FileName;
                string fullPath = CreateFilePath(initPath, copyNumber);

                using (var fileStream = new FileStream(fullPath, FileMode.Create))
                {
                    await attachment.CopyToAsync(fileStream);
                }
                Attachment file = new Attachment { Name = Path.GetFileName(fullPath), Path = fullPath, AssignmentId = assignmentId };
                await taskSystemDb.AttachmentRepository.AddAsync(file);
            }
            await taskSystemDb.SaveAsync();
        }

        public async Task DeleteAttachmentAsync(int attachmentId)
        {
            File.Delete(taskSystemDb.AttachmentRepository.GetByIdAsync(attachmentId).Result.Path);

            await Task.Run(() => taskSystemDb.AttachmentRepository.DeleteByIdAsync(attachmentId));
            await taskSystemDb.SaveAsync();
        }

        public IEnumerable<Attachment> GetAttachmentsByContainerId(int id)
        {
            return taskSystemDb.AssignmentRepository.GetByIdWithDetailsAsync(id).Result.Attachments.ToList();
        }

        private string CreateFilePath(string path, int copyNumber)
        {
            string newPath = path;
            string fileExtension = Path.GetExtension(path);
            string fileName = Path.GetFileNameWithoutExtension(path);
            string directoryName = Path.GetDirectoryName(path);

            if (taskSystemDb.AttachmentRepository.GetAll().Where(a => a.Path == path).FirstOrDefault() != null)
            {
                int newCopyNUmber = ++copyNumber;
                newPath = Path.Combine(directoryName, $"{fileName}({newCopyNUmber}){fileExtension}");
                return CreateFilePath(newPath, newCopyNUmber);
            }
            else
            {
                return newPath;
            }
        }
    }
}
