﻿using AutoMapper;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using TaskTrackingSystem.Business.Helpers;
using TaskTrackingSystem.Business.Interfaces;
using TaskTrackingSystem.Business.Models;
using TaskTrackingSystem.Business.Validation;
using TaskTrackingSystem.Data.Interfaces;

namespace TaskTrackingSystem.Business.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private IUnitOfWork taskSystemDb { get; set; }
        private Mapper mapper { get; set; }
        private AppSettings appSettings;
        public AuthenticationService(IUnitOfWork unitOfWork, Mapper mapper, IOptions<AppSettings> appSettings)
        {
            taskSystemDb = unitOfWork;
            this.mapper = mapper;
            this.appSettings = appSettings.Value;
        }

        public UserLoggedModel Authenticate(UserAuthModel userAuthModel)
        {
            var user = taskSystemDb.UserRepository.GetAllWithDetails().Where(u => u.Email == userAuthModel.Email && u.Password == userAuthModel.Password).FirstOrDefault();
            if (user == null)
                throw new IncorrectInputException("E-mail or password are invalid. Try again");

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim(ClaimTypes.Role, user.Role.Name)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            UserLoggedModel userLoggedModel = new UserLoggedModel { UserId = user.Id, Email = user.Email, Role = user.Role.Name, Token = tokenHandler.WriteToken(token) };

            return userLoggedModel;
        }
    }
}
