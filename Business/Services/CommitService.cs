﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTrackingSystem.Data.Entities;
using TaskTrackingSystem.Data.Interfaces;
using TaskTrackingSystem.Business.Interfaces;
using TaskTrackingSystem.Business.Models;

namespace TaskTrackingSystem.Business.Services
{
    public class CommitService : ICommitService
    {
        private IUnitOfWork taskSystemDb { get; set; }
        private Mapper mapper { get; set; }
        public CommitService(IUnitOfWork unitOfWork, Mapper mapper)
        {
            taskSystemDb = unitOfWork;
            this.mapper = mapper;
        }
        public async Task AddAsync(CommitModel model)
        {
            await taskSystemDb.CommitRepository.AddAsync(mapper.Map<CommitModel, Commit>(model));
            await taskSystemDb.SaveAsync();
        }
        public async Task DeleteByIdAsync(int modelId)
        {
            await Task.Run(() => taskSystemDb.CommitRepository.DeleteByIdAsync(modelId));
            await taskSystemDb.SaveAsync();
        }

        public IEnumerable<CommitModel> GetAll()
        {
            return mapper.Map<IEnumerable<Commit>, IEnumerable<CommitModel>>(taskSystemDb.CommitRepository.GetAllWithDetails().AsEnumerable());
        }

        public IEnumerable<CommitModel> GetByFilter(CommitFilterModel filterModel)
        {
            IQueryable<Commit> commits = taskSystemDb.CommitRepository.GetAllWithDetails();

            if (filterModel.AssignmentId != null)
                commits = commits.Where(p => p.AssignmentId == filterModel.AssignmentId);

            if (filterModel.Name != null)
                commits = commits.Where(p => p.Name.ToLower().Contains(filterModel.Name.ToLower()));

            if (filterModel.ContributorId != null)
                commits = commits.Where(p => p.ContributorId == filterModel.ContributorId);

            if (filterModel.CreationDate != null)
                commits = commits.Where(p => p.CreationDate == filterModel.CreationDate);

            return mapper.Map<IEnumerable<Commit>, IEnumerable<CommitModel>>(commits.AsEnumerable());
        }

        public async Task<CommitModel> GetByIdAsync(int id)
        {
            return await Task.Run(() => mapper.Map<Commit, CommitModel>(taskSystemDb.CommitRepository.GetByIdWithDetailsAsync(id).Result));
        }

        public async Task UpdateAsync(CommitModel model)
        {
            taskSystemDb.CommitRepository.Update(mapper.Map<CommitModel, Commit>(model));
            await taskSystemDb.SaveAsync();
        }

        public async Task<Attachment> GetAttachmentById(int id)
        {
            return await Task.Run(() => taskSystemDb.AttachmentRepository.GetByIdAsync(id));
        }

        public async Task AddAttachmentsAsync(IFormFileCollection attachments, int commitId, string path)
        {
            foreach (var attachment in attachments)
            {
                int copyNumber = 0;
                string initPath = path + attachment.FileName;
                string fullPath = CreateFilePath(initPath, copyNumber);

                using (var fileStream = new FileStream(fullPath, FileMode.Create))
                {
                    await attachment.CopyToAsync(fileStream);
                }
                Attachment file = new Attachment { Name = Path.GetFileName(fullPath), Path = fullPath, CommitId = commitId };
                await taskSystemDb.AttachmentRepository.AddAsync(file);
            }
            await taskSystemDb.SaveAsync();
        }

        public async Task DeleteAttachmentAsync(int attachmentId)
        {
            File.Delete(taskSystemDb.AttachmentRepository.GetByIdAsync(attachmentId).Result.Path);

            await Task.Run(() => taskSystemDb.AttachmentRepository.DeleteByIdAsync(attachmentId));
            await taskSystemDb.SaveAsync();
        }

        public IEnumerable<Attachment> GetAttachmentsByContainerId(int id)
        {
            return taskSystemDb.CommitRepository.GetByIdWithDetailsAsync(id).Result.Attachments.ToList();
        }

        private string CreateFilePath(string path, int copyNumber)
        {
            string newPath = path;
            string fileExtension = Path.GetExtension(path);
            string fileName = Path.GetFileNameWithoutExtension(path);
            string directoryName = Path.GetDirectoryName(path);

            if (taskSystemDb.AttachmentRepository.GetAll().Where(a => a.Path == path).FirstOrDefault() != null)
            {
                int newCopyNUmber = ++copyNumber;
                newPath = Path.Combine(directoryName, $"{fileName}({newCopyNUmber}){fileExtension}");
                return CreateFilePath(newPath, newCopyNUmber);
            }
            else
            {
                return newPath;
            }
        }
    }
}
