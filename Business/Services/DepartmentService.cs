﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTrackingSystem.Data.Entities;
using TaskTrackingSystem.Data.Interfaces;
using TaskTrackingSystem.Business.Interfaces;
using TaskTrackingSystem.Business.Models;

namespace TaskTrackingSystem.Business.Services
{
    public class DepartmentService : IDepartmentService
    {
        private IUnitOfWork taskSystemDb { get; set; }
        private Mapper mapper { get; set; }
        public DepartmentService(IUnitOfWork unitOfWork, Mapper mapper)
        {
            taskSystemDb = unitOfWork;
            this.mapper = mapper;
        }

        public async Task AddAsync(DepartmentModel model)
        {
            await taskSystemDb.DepatmentRepository.AddAsync(mapper.Map<DepartmentModel, Department>(model));
            await taskSystemDb.SaveAsync();
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            await Task.Run(() => taskSystemDb.DepatmentRepository.DeleteByIdAsync(modelId));
            await taskSystemDb.SaveAsync();
        }

        public IEnumerable<DepartmentModel> GetAll()
        {
            return mapper.Map<IEnumerable<Department>, IEnumerable<DepartmentModel>>(taskSystemDb.DepatmentRepository.GetAllWithDetails().AsEnumerable());
        }

        public async Task<DepartmentModel> GetByIdAsync(int id)
        {
            return await Task.Run(() => mapper.Map<Department, DepartmentModel>(taskSystemDb.DepatmentRepository.GetByIdWithDetailsAsync(id).Result));
        }

        public async Task UpdateAsync(DepartmentModel model)
        {
            taskSystemDb.DepatmentRepository.Update(mapper.Map<DepartmentModel, Department>(model));
            await taskSystemDb.SaveAsync();
        }
    }
}
