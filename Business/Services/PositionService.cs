﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTrackingSystem.Data.Entities;
using TaskTrackingSystem.Data.Interfaces;
using TaskTrackingSystem.Business.Interfaces;
using TaskTrackingSystem.Business.Models;

namespace TaskTrackingSystem.Business.Services
{
    public class PositionService : IPositionService
    {
        private IUnitOfWork taskSystemDb { get; set; }
        private Mapper mapper { get; set; }
        public PositionService(IUnitOfWork unitOfWork, Mapper mapper)
        {
            taskSystemDb = unitOfWork;
            this.mapper = mapper;
        }

        public async Task AddAsync(PositionModel model)
        {
            await taskSystemDb.PositionRepository.AddAsync(mapper.Map<PositionModel, Position>(model));
            await taskSystemDb.SaveAsync();
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            await Task.Run(() => taskSystemDb.PositionRepository.DeleteByIdAsync(modelId));
            await taskSystemDb.SaveAsync();
        }

        public IEnumerable<PositionModel> GetAll()
        {
            return mapper.Map<IEnumerable<Position>, IEnumerable<PositionModel>>(taskSystemDb.PositionRepository.GetAllWithDetails().AsEnumerable());
        }

        public async Task<PositionModel> GetByIdAsync(int id)
        {
            return await Task.Run(() => mapper.Map<Position, PositionModel>(taskSystemDb.PositionRepository.GetByIdWithDetailsAsync(id).Result));
        }

        public async Task UpdateAsync(PositionModel model)
        {
            taskSystemDb.PositionRepository.Update(mapper.Map<PositionModel, Position>(model));
            await taskSystemDb.SaveAsync();
        }
    }
}
