﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTrackingSystem.Data.Entities;
using TaskTrackingSystem.Data.Interfaces;
using TaskTrackingSystem.Business.Interfaces;
using TaskTrackingSystem.Business.Models;
using TaskTrackingSystem.Business.Validation;
using System.IO;

namespace TaskTrackingSystem.Business.Services
{
    public class ProjectService : IProjectService
    {
        private IUnitOfWork taskSystemDb { get; set; }
        private Mapper mapper { get; set; }
        public ProjectService(IUnitOfWork unitOfWork, Mapper mapper)
        {
            taskSystemDb = unitOfWork;
            this.mapper = mapper;
        }

        public async Task AddAsync(ProjectModel model)
        {
            if (model.DeadLine < model.CreationDate)
                throw new IncorrectInputException("Deadline can't be in the past");

            await taskSystemDb.ProjectRepository.AddAsync(mapper.Map<ProjectModel, Project>(model));
            await taskSystemDb.SaveAsync();
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            await Task.Run(() => taskSystemDb.ProjectRepository.DeleteByIdAsync(modelId));
            await taskSystemDb.SaveAsync();
        }

        public IEnumerable<ProjectModel> GetAll()
        {
            return mapper.Map<IEnumerable<Project>, IEnumerable<ProjectModel>>(taskSystemDb.ProjectRepository.GetAllWithDetails().AsEnumerable());
        }

        public IEnumerable<ProjectModel> GetByFilter(ProjectFilterModel filterModel)
        {
            IQueryable<Project> projects = taskSystemDb.ProjectRepository.GetAllWithDetails();

            if (filterModel.CreatorId != null)
                projects = projects.Where(p => p.CreatorId == filterModel.CreatorId);

            if (filterModel.CreationDate != null)
                projects = projects.Where(p => p.CreationDate == filterModel.CreationDate);

            if (filterModel.DeadLine != null)
                projects = projects.Where(p => p.DeadLine == filterModel.DeadLine);

            if (filterModel.Name != null)
                projects = projects.Where(p => p.Name.ToLower().Contains(filterModel.Name.ToLower()));

            if (filterModel.ProjectStatusId != null)
                projects = projects.Where(p => p.ProjectStatusId == filterModel.ProjectStatusId);

            if (filterModel.ContributorsIds != null)
                projects = projects.Where(p => p.Contributors.Select(c => c.Id).Intersect(filterModel.ContributorsIds).Count() != 0);

            return mapper.Map<IEnumerable<Project>, IEnumerable<ProjectModel>>(projects.AsEnumerable());
        }

        public async Task<ProjectModel> GetByIdAsync(int id)
        {
            return await Task.Run(() => mapper.Map<Project, ProjectModel>(taskSystemDb.ProjectRepository.GetByIdWithDetailsAsync(id).Result));
        }

        public async Task UpdateAsync(ProjectModel model)
        {
            if (model.DeadLine < model.CreationDate)
                throw new IncorrectInputException("Deadline can't be in the past");

            taskSystemDb.ProjectRepository.Update(mapper.Map<ProjectModel, Project>(model));
            await taskSystemDb.SaveAsync();
        }

        public async Task<Attachment> GetAttachmentById(int id)
        {
            return await Task.Run(() => taskSystemDb.AttachmentRepository.GetByIdAsync(id));
        }

        public async Task AddAttachmentsAsync(IFormFileCollection attachments, int projectId, string path)
        {
            foreach (var attachment in attachments)
            {
                int copyNumber = 0;
                string initPath = path + attachment.FileName;
                string fullPath = CreateFilePath(initPath, copyNumber);

                using (var fileStream = new FileStream(fullPath, FileMode.Create))
                {
                    await attachment.CopyToAsync(fileStream);
                }
                Attachment file = new Attachment { Name = Path.GetFileName(fullPath), Path = fullPath, ProjectId = projectId };
                await taskSystemDb.AttachmentRepository.AddAsync(file);
            }
            await taskSystemDb.SaveAsync();
        }

        public async Task DeleteAttachmentAsync(int attachmentId)
        {
            File.Delete(taskSystemDb.AttachmentRepository.GetByIdAsync(attachmentId).Result.Path);

            await Task.Run(() => taskSystemDb.AttachmentRepository.DeleteByIdAsync(attachmentId));
            await taskSystemDb.SaveAsync();
        }

        public IEnumerable<Attachment> GetAttachmentsByContainerId(int id)
        {
            return taskSystemDb.ProjectRepository.GetByIdWithDetailsAsync(id).Result.Attachments.ToList();
        }

        private string CreateFilePath(string path, int copyNumber)
        {
            string newPath = path;
            string fileExtension = Path.GetExtension(path);
            string fileName = Path.GetFileNameWithoutExtension(path);
            string directoryName = Path.GetDirectoryName(path);

            if (taskSystemDb.AttachmentRepository.GetAll().Where(a => a.Path == path).FirstOrDefault() != null)
            {
                int newCopyNUmber = ++copyNumber;
                newPath = Path.Combine(directoryName, $"{fileName}({newCopyNUmber}){fileExtension}");
                return CreateFilePath(newPath, newCopyNUmber);
            }
            else
            {
                return newPath;
            }
        }
    }
}
