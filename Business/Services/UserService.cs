﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTrackingSystem.Data;
using TaskTrackingSystem.Data.Entities;
using TaskTrackingSystem.Data.Interfaces;
using TaskTrackingSystem.Business.Interfaces;
using TaskTrackingSystem.Business.Models;
using TaskTrackingSystem.Business.Validation;

namespace TaskTrackingSystem.Business.Services
{
    public class UserService : IUserService
    {
        private IUnitOfWork taskSystemDb { get; set; }
        private Mapper mapper { get; set; }
        public UserService(IUnitOfWork unitOfWork, Mapper mapper)
        {
            taskSystemDb = unitOfWork;
            this.mapper = mapper;
        }

        public async Task AddAsync(UserModel model)
        {
            if (model.NewPositionId == null)
                throw new IncorrectInputException("You have to choose position for this User");
            else
                await taskSystemDb.UserRepository.AddAsync(mapper.Map<UserModel, User>(model));
            await taskSystemDb.PositionRecordRepository.AddAsync(
                new PositionRecord
                {
                    UserId = taskSystemDb.UserRepository.GetAllWithDetails().Last().Id,
                    FirstDay = DateTime.Now,
                    PositionId = model.NewPositionId.Value
                });
            await taskSystemDb.SaveAsync();
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            await Task.Run(() => taskSystemDb.UserRepository.DeleteByIdAsync(modelId));
            await taskSystemDb.SaveAsync();
        }

        public IEnumerable<UserModel> GetAll()
        {
            return mapper.Map<IEnumerable<User>, IEnumerable<UserModel>>(taskSystemDb.UserRepository.GetAllWithDetails().AsEnumerable());
        }

        public async Task<UserModel> GetByIdAsync(int id)
        {
            return await Task.Run(() => mapper.Map<User, UserModel>(taskSystemDb.UserRepository.GetByIdWithDetailsAsync(id).Result));
        }

        public async Task UpdateAsync(UserModel model)
        {
            if (taskSystemDb.UserRepository.GetAll().AsEnumerable().SingleOrDefault(u => u.Email == model.Email) != null)
                throw new IncorrectInputException($"Email {model.Email} is already in use");

            taskSystemDb.UserRepository.Update(mapper.Map<UserModel, User>(model));
            await taskSystemDb.SaveAsync();
        }

        public async Task ChangeUserPositionAsync(PositionRecordModel newPositioRecordModel)
        {
            List<PositionRecord> positionRecords = taskSystemDb.PositionRecordRepository.GetAllWithDetails().ToList();

            User busyPosition = positionRecords.Where(pr => pr.PositionId == newPositioRecordModel.PositionId && pr.LastDay == null).LastOrDefault().User;

            if (busyPosition != null)
                throw new IncorrectInputException($"You can't appoint to this position, it's already occupied by {busyPosition.Id}");

            PositionRecord oldPositionRecord = positionRecords.Where(pr => pr.UserId == newPositioRecordModel.UserId).LastOrDefault();

            oldPositionRecord.LastDay = newPositioRecordModel.FirstDay.AddDays(-1);

            taskSystemDb.PositionRecordRepository.Update(oldPositionRecord);

            await taskSystemDb.PositionRecordRepository.AddAsync(mapper.Map<PositionRecordModel, PositionRecord>(newPositioRecordModel));

            await taskSystemDb.SaveAsync();
        }

        public IEnumerable<PositionRecordModel> GetUserPositionsHistory(int userId)
        {
            return mapper.Map<IEnumerable<PositionRecord>, IEnumerable<PositionRecordModel>>(taskSystemDb.PositionRecordRepository.GetAll().Where(pr => pr.UserId == userId));
        }
    }
}
