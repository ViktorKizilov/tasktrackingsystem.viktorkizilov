﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskTrackingSystem.Business.Validation
{
    [Serializable]
    public class IncorrectInputException : Exception
    {
        public IncorrectInputException(string message) : base(message) { }
    }
}
