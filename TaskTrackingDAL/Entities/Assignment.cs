﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TaskTrackingSystem.Data.Entities
{
    public class Assignment : BaseEntity
    {
        public int ProjectId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime DeadLine { get; set; }
        public DateTime StatusChangedDate { get; set; }
        public int? StatusId { get; set; }
        public virtual AssignmentStatus Status { get; set; }

        public int? CreatorId { get; set; }
        public virtual User Creator { get; set; }

        public int? ExecutorId { get; set; }
        public virtual User Executor { get; set; }

        public virtual ICollection<Attachment> Attachments { get; set; }
        public virtual ICollection<Commit> Commits { get; set; }
    }
}
