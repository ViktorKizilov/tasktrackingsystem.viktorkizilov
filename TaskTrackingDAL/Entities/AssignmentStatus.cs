﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskTrackingSystem.Data.Entities
{
    public class AssignmentStatus : BaseEntity
    {
        public string Name { get; set; }
        public virtual ICollection<Assignment> Assignments { get; set; }
    }
}
