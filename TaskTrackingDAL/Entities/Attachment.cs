﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskTrackingSystem.Data.Entities
{
    public class Attachment : BaseEntity
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public int? ProjectId { get; set; }
        public int? AssignmentId { get; set; }
        public int? CommitId { get; set; }
        public virtual Project Project { get; set; }
        public virtual Assignment Assignment { get; set; }
        public virtual Commit Commit { get; set; }
    }
}
