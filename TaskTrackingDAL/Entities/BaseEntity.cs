﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TaskTrackingSystem.Data
{
    public abstract class BaseEntity
    {
        [Key]
        public int Id { get; set; }
    }
}
