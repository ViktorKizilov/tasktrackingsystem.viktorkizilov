﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskTrackingSystem.Data.Entities
{
    public class Commit : BaseEntity
    {
        public int AssignmentId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? ContributorId { get; set; }
        public DateTime CreationDate { get; set; }
        public virtual ICollection<Attachment> Attachments { get; set; }
        public virtual Assignment Assignment { get; set; }
        public virtual User Contributor { get; set; }
    }
}
