﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TaskTrackingSystem.Data.Entities
{
    public class Department : BaseEntity
    {
        public string Name { get; set; }
        public int? BossDepartmentId { get; set; }
        public virtual Department BossDepartment { get; set; }
        public virtual ICollection<Position> Positions { get; set; }
    }
}
