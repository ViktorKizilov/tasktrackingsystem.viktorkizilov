﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TaskTrackingSystem.Data.Entities
{
    public class Position : BaseEntity
    {
        public string Name { get; set; }
        public int? BossPositionId { get; set; }
        public int DepartmentId { get; set; }
        public virtual Position BossPosition { get; set; }
        public virtual Department Department { get; set; }
        public virtual ICollection<PositionRecord> PositionRecords{ get; set; }
    }
}
