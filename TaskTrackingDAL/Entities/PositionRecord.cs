﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TaskTrackingSystem.Data.Entities
{
    public class PositionRecord : BaseEntity
    {
        public int UserId { get; set; }
        public int? PositionId { get; set; }
        public DateTime FirstDay { get; set; }
        public DateTime? LastDay { get; set; }
        public virtual User User { get; set; }
        public virtual Position Position { get; set; }
    }
}
