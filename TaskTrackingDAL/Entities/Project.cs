﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TaskTrackingSystem.Data.Entities
{
    public class Project : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime DeadLine { get; set; }
        public DateTime StatusChangedDate { get; set; }

        public int? ProjectStatusId { get; set; }
        public virtual ProjectStatus Status { get; set; }

        public int? CreatorId { get; set; }
        public virtual User Creator { get; set; }

        public virtual ICollection<Attachment> Attachments { get; set; }
        public virtual ICollection<User> Contributors { get; set; }
        public virtual ICollection<Assignment> Assignments { get; set; }
    }
}
