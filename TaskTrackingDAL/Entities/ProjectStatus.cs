﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskTrackingSystem.Data.Entities
{
    public class ProjectStatus : BaseEntity
    {
        public string Name { get; set; }
        public virtual ICollection<Project> Projects { get; set; }
    }
}
