﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TaskTrackingSystem.Data.Entities
{
    public class User : BaseEntity
    {
        public string Name { get; set; }
        public string Surname { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
        public DateTime BirthDate { get; set; }
        public int? RoleId { get; set; }
        public virtual Role Role { get; set; }

        public virtual ICollection<PositionRecord> PositionsHistory { get; set; }

        [InverseProperty("Creator")]
        public virtual ICollection<Project> CreatedProjects { get; set; }

        public virtual ICollection<Project> ContributedProjects { get; set; }

        [InverseProperty("Creator")]
        public virtual ICollection<Assignment> CreatedAssignments { get; set; }

        [InverseProperty("Executor")]
        public virtual ICollection<Assignment> ContributedAssignments { get; set; }
    }
}
