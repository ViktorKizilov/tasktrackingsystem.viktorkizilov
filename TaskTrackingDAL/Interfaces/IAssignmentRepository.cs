﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTrackingSystem.Data.Entities;

namespace TaskTrackingSystem.Data.Interfaces
{
    public interface IAssignmentRepository : IRepository<Assignment>
    {
        IQueryable<Assignment> GetAllWithDetails();
        Task<Assignment> GetByIdWithDetailsAsync(int id);
    }
}
