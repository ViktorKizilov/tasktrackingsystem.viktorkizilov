﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTrackingSystem.Data.Entities;

namespace TaskTrackingSystem.Data.Interfaces
{
    public interface IAttachmentRepository
    {
        Task<Attachment> GetByIdAsync(int id);
        IQueryable<Attachment> GetAll();
        Task AddAsync(Attachment entity);
        Task DeleteByIdAsync(int id);
    }
}
