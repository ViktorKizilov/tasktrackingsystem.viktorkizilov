﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTrackingSystem.Data.Entities;

namespace TaskTrackingSystem.Data.Interfaces
{
    public interface ICommitRepository : IRepository<Commit>
    {
        IQueryable<Commit> GetAllWithDetails();
        Task<Commit> GetByIdWithDetailsAsync(int id);
    }
}
