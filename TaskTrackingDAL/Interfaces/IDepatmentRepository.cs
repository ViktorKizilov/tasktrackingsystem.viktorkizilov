﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTrackingSystem.Data.Entities;

namespace TaskTrackingSystem.Data.Interfaces
{
    public interface IDepatmentRepository : IRepository<Department>
    {
        IQueryable<Department> GetAllWithDetails();
        Task<Department> GetByIdWithDetailsAsync(int id);
    }
}
