﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTrackingSystem.Data.Entities;

namespace TaskTrackingSystem.Data.Interfaces
{
    public interface IPositionRecordRepository : IRepository<PositionRecord>
    {
        IQueryable<PositionRecord> GetAllWithDetails();
        Task<PositionRecord> GetByIdWithDetailsAsync(int id);
    }
}
