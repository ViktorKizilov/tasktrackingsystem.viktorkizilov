﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTrackingSystem.Data.Entities;

namespace TaskTrackingSystem.Data.Interfaces
{
    public interface IPositionRepository : IRepository<Position>
    {
        IQueryable<Position> GetAllWithDetails();
        Task<Position> GetByIdWithDetailsAsync(int id);
    }
}
