﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTrackingSystem.Data.Entities;

namespace TaskTrackingSystem.Data.Interfaces
{
    public interface IProjectRepository : IRepository<Project>
    {
        IQueryable<Project> GetAllWithDetails();
        Task<Project> GetByIdWithDetailsAsync(int id);
    }
}
