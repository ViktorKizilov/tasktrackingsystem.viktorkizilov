﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TaskTrackingSystem.Data.Interfaces
{
    public interface IUnitOfWork
    {
        IAttachmentRepository AttachmentRepository { get; }
        IAssignmentRepository AssignmentRepository { get; }
        ICommitRepository CommitRepository { get; }
        IDepatmentRepository DepatmentRepository { get; }
        IPositionRecordRepository PositionRecordRepository { get; }
        IPositionRepository PositionRepository { get; }
        IProjectRepository ProjectRepository { get; }
        IUserRepository UserRepository { get; }
        Task<int> SaveAsync();
    }
}
