﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTrackingSystem.Data.Entities;

namespace TaskTrackingSystem.Data.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        IQueryable<User> GetAllWithDetails();
        Task<User> GetByIdWithDetailsAsync(int id);
    }
}
