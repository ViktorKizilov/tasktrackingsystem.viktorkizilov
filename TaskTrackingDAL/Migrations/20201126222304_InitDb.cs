﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TaskTrackingSystem.Data.Migrations
{
    public partial class InitDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            /*
            migrationBuilder.CreateTable(
                name: "AssignmentStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssignmentStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Departments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BossDepartmentId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Departments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Departments_Departments_BossDepartmentId",
                        column: x => x.BossDepartmentId,
                        principalTable: "Departments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProjectStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Positions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BossPositionId = table.Column<int>(type: "int", nullable: true),
                    DepartmentId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Positions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Positions_Departments_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Departments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Positions_Positions_BossPositionId",
                        column: x => x.BossPositionId,
                        principalTable: "Positions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Surname = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BirthDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    RoleId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PositionRecords",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    PositionId = table.Column<int>(type: "int", nullable: true),
                    FirstDay = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastDay = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PositionRecords", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PositionRecords_Positions_PositionId",
                        column: x => x.PositionId,
                        principalTable: "Positions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PositionRecords_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Projects",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeadLine = table.Column<DateTime>(type: "datetime2", nullable: false),
                    StatusChangedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ProjectStatusId = table.Column<int>(type: "int", nullable: true),
                    CreatorId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Projects_ProjectStatuses_ProjectStatusId",
                        column: x => x.ProjectStatusId,
                        principalTable: "ProjectStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Projects_Users_CreatorId",
                        column: x => x.CreatorId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Assignments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProjectId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeadLine = table.Column<DateTime>(type: "datetime2", nullable: false),
                    StatusChangedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    StatusId = table.Column<int>(type: "int", nullable: true),
                    CreatorId = table.Column<int>(type: "int", nullable: true),
                    ExecutorId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Assignments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Assignments_AssignmentStatuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "AssignmentStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Assignments_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Assignments_Users_CreatorId",
                        column: x => x.CreatorId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Assignments_Users_ExecutorId",
                        column: x => x.ExecutorId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProjectUser",
                columns: table => new
                {
                    ContributedProjectsId = table.Column<int>(type: "int", nullable: false),
                    ContributorsId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectUser", x => new { x.ContributedProjectsId, x.ContributorsId });
                    table.ForeignKey(
                        name: "FK_ProjectUser_Projects_ContributedProjectsId",
                        column: x => x.ContributedProjectsId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProjectUser_Users_ContributorsId",
                        column: x => x.ContributorsId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Commits",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AssignmentId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ContributorId = table.Column<int>(type: "int", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Commits", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Commits_Assignments_AssignmentId",
                        column: x => x.AssignmentId,
                        principalTable: "Assignments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Commits_Users_ContributorId",
                        column: x => x.ContributorId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Attachments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Path = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ProjectId = table.Column<int>(type: "int", nullable: true),
                    AssignmentId = table.Column<int>(type: "int", nullable: true),
                    CommitId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Attachments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Attachments_Assignments_AssignmentId",
                        column: x => x.AssignmentId,
                        principalTable: "Assignments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Attachments_Commits_CommitId",
                        column: x => x.CommitId,
                        principalTable: "Commits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Attachments_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });*/

            migrationBuilder.InsertData(
                table: "AssignmentStatuses",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Working on" },
                    { 2, "On revision" },
                    { 3, "Returned after revision" },
                    { 4, "Submitted" }
                });

            migrationBuilder.InsertData(
                table: "Departments",
                columns: new[] { "Id", "BossDepartmentId", "Name" },
                values: new object[] { 1, null, "Management" });

            migrationBuilder.InsertData(
                table: "ProjectStatuses",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Open" },
                    { 2, "Revision" },
                    { 3, "Implementing" },
                    { 4, "Succeeded" },
                    { 5, "Failed" }
                });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Admin" },
                    { 2, "User" }
                });

            migrationBuilder.InsertData(
                table: "Departments",
                columns: new[] { "Id", "BossDepartmentId", "Name" },
                values: new object[,]
                {
                    { 2, 1, "Marketing" },
                    { 3, 1, "Production" },
                    { 4, 1, "R&D" }
                });

            migrationBuilder.InsertData(
                table: "Positions",
                columns: new[] { "Id", "BossPositionId", "DepartmentId", "Name" },
                values: new object[] { 1, null, 1, "CEO" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDate", "Email", "Name", "Password", "RoleId", "Surname" },
                values: new object[,]
                {
                    { 1, new DateTime(1995, 5, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), "v.kizilov@tasksystem.com", "Viktor", "trial", 1, "Kizilov" },
                    { 2, new DateTime(1955, 10, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "b.gates@tasksystem.com", "Bill", "trial2", 2, "Gates" },
                    { 3, new DateTime(1971, 6, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "e.mask@tasksystem.com", "Elon", "trial3", 2, "Musk" },
                    { 4, new DateTime(1991, 6, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "g.gershwin@tasksystem.com", "George", "trial4", 2, "Gershwin" },
                    { 5, new DateTime(1990, 1, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "o.peterson@tasksystem.com", "Oscar", "trial5", 2, "Peterson" }
                });

            migrationBuilder.InsertData(
                table: "Departments",
                columns: new[] { "Id", "BossDepartmentId", "Name" },
                values: new object[,]
                {
                    { 5, 2, "Trade Marketing" },
                    { 6, 2, "Brand Management" },
                    { 7, 3, "Factory management" }
                });

            migrationBuilder.InsertData(
                table: "PositionRecords",
                columns: new[] { "Id", "FirstDay", "LastDay", "PositionId", "UserId" },
                values: new object[] { 1, new DateTime(2019, 11, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 1, 1 });

            migrationBuilder.InsertData(
                table: "Positions",
                columns: new[] { "Id", "BossPositionId", "DepartmentId", "Name" },
                values: new object[,]
                {
                    { 2, 1, 2, "Marketing Director" },
                    { 3, 1, 3, "Production Director" },
                    { 4, 1, 4, "Development Director" }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "CreationDate", "CreatorId", "DeadLine", "Description", "Name", "ProjectStatusId", "StatusChangedDate" },
                values: new object[,]
                {
                    { 1, new DateTime(2019, 11, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, new DateTime(2019, 12, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), "FirstProject Description", "FirstProject", 1, new DateTime(2019, 11, 17, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, new DateTime(2019, 12, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, new DateTime(2019, 12, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), "SecondProject Description", "SecondProject", 4, new DateTime(2020, 5, 12, 0, 0, 0, 0, DateTimeKind.Unspecified) }
                });

            migrationBuilder.InsertData(
                table: "Assignments",
                columns: new[] { "Id", "CreationDate", "CreatorId", "DeadLine", "Description", "ExecutorId", "Name", "ProjectId", "StatusChangedDate", "StatusId" },
                values: new object[] { 1, new DateTime(2019, 11, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, new DateTime(2019, 12, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), "Create marketing plan for 2020", 2, "Marketing plan", 1, new DateTime(2019, 12, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), 4 });

            migrationBuilder.InsertData(
                table: "Departments",
                columns: new[] { "Id", "BossDepartmentId", "Name" },
                values: new object[] { 8, 5, "Modern Trade" });

            migrationBuilder.InsertData(
                table: "PositionRecords",
                columns: new[] { "Id", "FirstDay", "LastDay", "PositionId", "UserId" },
                values: new object[,]
                {
                    { 2, new DateTime(2019, 11, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2019, 12, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, 2 },
                    { 4, new DateTime(2019, 12, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 2, 3 },
                    { 3, new DateTime(2019, 12, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 3, 2 }
                });

            migrationBuilder.InsertData(
                table: "Positions",
                columns: new[] { "Id", "BossPositionId", "DepartmentId", "Name" },
                values: new object[,]
                {
                    { 5, 2, 5, "Channel Manager" },
                    { 7, 2, 6, "Shopper Marketing Manager" }
                });

            migrationBuilder.InsertData(
                table: "Commits",
                columns: new[] { "Id", "AssignmentId", "ContributorId", "CreationDate", "Description", "Name" },
                values: new object[] { 1, 1, 2, new DateTime(2019, 12, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), "Presentation in PDF format", "Marketing plan 2020" });

            migrationBuilder.InsertData(
                table: "PositionRecords",
                columns: new[] { "Id", "FirstDay", "LastDay", "PositionId", "UserId" },
                values: new object[] { 5, new DateTime(2019, 11, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 5, 4 });

            migrationBuilder.InsertData(
                table: "Positions",
                columns: new[] { "Id", "BossPositionId", "DepartmentId", "Name" },
                values: new object[] { 6, 5, 8, "Modern Trade Manager" });

            migrationBuilder.InsertData(
                table: "Attachments",
                columns: new[] { "Id", "AssignmentId", "CommitId", "Name", "Path", "ProjectId" },
                values: new object[] { 1, null, 1, "Marketing plan 2020", "C:\\Users\\User999\\Desktop\\EPAM_courses\\Final_task\\TaskTracking\\WebAPI\\AttachedFiles\\Marketing plan 2020.pdf", null });

            migrationBuilder.InsertData(
                table: "PositionRecords",
                columns: new[] { "Id", "FirstDay", "LastDay", "PositionId", "UserId" },
                values: new object[] { 6, new DateTime(2019, 11, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 6, 5 });

            migrationBuilder.InsertData(
                table: "Positions",
                columns: new[] { "Id", "BossPositionId", "DepartmentId", "Name" },
                values: new object[] { 8, 6, 8, "Modern Trade Specialist" });
            /*
            migrationBuilder.CreateIndex(
                name: "IX_Assignments_CreatorId",
                table: "Assignments",
                column: "CreatorId");

            migrationBuilder.CreateIndex(
                name: "IX_Assignments_ExecutorId",
                table: "Assignments",
                column: "ExecutorId");

            migrationBuilder.CreateIndex(
                name: "IX_Assignments_ProjectId",
                table: "Assignments",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Assignments_StatusId",
                table: "Assignments",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Attachments_AssignmentId",
                table: "Attachments",
                column: "AssignmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Attachments_CommitId",
                table: "Attachments",
                column: "CommitId");

            migrationBuilder.CreateIndex(
                name: "IX_Attachments_ProjectId",
                table: "Attachments",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Commits_AssignmentId",
                table: "Commits",
                column: "AssignmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Commits_ContributorId",
                table: "Commits",
                column: "ContributorId");

            migrationBuilder.CreateIndex(
                name: "IX_Departments_BossDepartmentId",
                table: "Departments",
                column: "BossDepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_PositionRecords_PositionId",
                table: "PositionRecords",
                column: "PositionId");

            migrationBuilder.CreateIndex(
                name: "IX_PositionRecords_UserId",
                table: "PositionRecords",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Positions_BossPositionId",
                table: "Positions",
                column: "BossPositionId");

            migrationBuilder.CreateIndex(
                name: "IX_Positions_DepartmentId",
                table: "Positions",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_CreatorId",
                table: "Projects",
                column: "CreatorId");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_ProjectStatusId",
                table: "Projects",
                column: "ProjectStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectUser_ContributorsId",
                table: "ProjectUser",
                column: "ContributorsId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_RoleId",
                table: "Users",
                column: "RoleId");
            */
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {/*
            migrationBuilder.DropTable(
                name: "Attachments");

            migrationBuilder.DropTable(
                name: "PositionRecords");

            migrationBuilder.DropTable(
                name: "ProjectUser");

            migrationBuilder.DropTable(
                name: "Commits");

            migrationBuilder.DropTable(
                name: "Positions");

            migrationBuilder.DropTable(
                name: "Assignments");

            migrationBuilder.DropTable(
                name: "Departments");

            migrationBuilder.DropTable(
                name: "AssignmentStatuses");

            migrationBuilder.DropTable(
                name: "Projects");

            migrationBuilder.DropTable(
                name: "ProjectStatuses");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Roles");
        */}
    }
}
