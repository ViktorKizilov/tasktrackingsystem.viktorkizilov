﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTrackingSystem.Data.Entities;
using TaskTrackingSystem.Data.Interfaces;

namespace TaskTrackingSystem.Data.Repositories
{
    public class AssignmentRepository : IAssignmentRepository
    {
        private readonly TaskSystemDBContext dbContext;
        public AssignmentRepository(TaskSystemDBContext dbContext) => this.dbContext = dbContext;

        public async Task AddAsync(Assignment entity)
        {
            await Task.Run(() => dbContext.Assignments.Add(entity));
        }

        public void Delete(Assignment entity)
        {
            dbContext.Assignments.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            Assignment assignment = dbContext.Assignments.Find(id);
            if (assignment != null)
            {
                foreach (var attachent in assignment.Attachments)
                    await Task.Run(() => dbContext.Attachments.Remove(attachent));
            }
            await Task.Run(() => dbContext.Assignments.Remove(assignment));
        }

        public IQueryable<Assignment> GetAll()
        {
            return dbContext.Assignments;
        }

        public IQueryable<Assignment> GetAllWithDetails()
        {
            return dbContext.Assignments
                .Include(a => a.Attachments)
                .Include(a => a.Commits)
                .Include(a => a.Creator)
                .Include(a => a.Executor)
                .Include(a => a.Status);
        }

        public async Task<Assignment> GetByIdAsync(int id)
        {
            return await Task.Run(() => dbContext.Assignments.FirstOrDefaultAsync(a => a.Id == id));
        }

        public async Task<Assignment> GetByIdWithDetailsAsync(int id)
        {
            return await Task.Run(() => dbContext.Assignments
            .Include(a => a.Attachments)
            .Include(a => a.Commits)
            .Include(a => a.Creator)
            .Include(a => a.Executor)
            .Include(a => a.Status)
            .FirstOrDefaultAsync(a => a.Id == id));
        }

        public void Update(Assignment entity)
        {
            dbContext.Entry(entity).State = EntityState.Modified;
        }
    }
}
