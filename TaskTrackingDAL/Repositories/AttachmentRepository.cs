﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTrackingSystem.Data;
using TaskTrackingSystem.Data.Entities;
using TaskTrackingSystem.Data.Interfaces;

namespace TaskTrackingSystem.Data.Repositories
{
    public class AttachmentRepository : IAttachmentRepository
    {
        private readonly TaskSystemDBContext dbContext;
        public AttachmentRepository(TaskSystemDBContext dbContext) => this.dbContext = dbContext;
        public async Task<Attachment> GetByIdAsync(int id)
        {
            return await Task.Run(() => dbContext.Attachments.FirstOrDefault(d => d.Id == id));
        }

        public async Task AddAsync(Attachment entity)
        {
            await Task.Run(() => dbContext.Attachments.Add(entity));
        }

        public async Task DeleteByIdAsync(int id)
        {
            Attachment attachment = dbContext.Attachments.Find(id);
            if (attachment != null)
                await Task.Run(() => dbContext.Attachments.Remove(attachment));
        }

        public IQueryable<Attachment> GetAll()
        {
            return dbContext.Attachments;
        }
    }
}
