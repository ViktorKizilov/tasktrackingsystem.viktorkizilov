﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTrackingSystem.Data.Entities;
using TaskTrackingSystem.Data.Interfaces;

namespace TaskTrackingSystem.Data.Repositories
{
    public class CommitRepository : ICommitRepository
    {
        private readonly TaskSystemDBContext dbContext;
        public CommitRepository(TaskSystemDBContext dbContext) => this.dbContext = dbContext;

        public async Task AddAsync(Commit entity)
        {
            await Task.Run(() => dbContext.Commits.Add(entity));
        }

        public void Delete(Commit entity)
        {
            dbContext.Commits.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            Commit commit = dbContext.Commits.Find(id);
            if (commit != null)
            {
                foreach (var attachent in commit.Attachments)
                    await Task.Run(() => dbContext.Attachments.Remove(attachent));
            }

            await Task.Run(() => dbContext.Commits.Remove(commit));
        }

        public IQueryable<Commit> GetAll()
        {
            return dbContext.Commits;
        }
        public IQueryable<Commit> GetAllWithDetails()
        {
            return dbContext.Commits
                .Include(c => c.Attachments)
                .Include(c => c.Contributor)
                .Include(c => c.Assignment);
        }

        public async Task<Commit> GetByIdAsync(int id)
        {
            return await Task.Run(() => dbContext.Commits.FirstOrDefault(Commit => Commit.Id == id));
        }

        public async Task<Commit> GetByIdWithDetailsAsync(int id)
        {
            return await Task.Run(() => dbContext.Commits
            .Include(c => c.Assignment)
            .Include(c => c.Attachments)
            .Include(c => c.Contributor)
            .FirstOrDefaultAsync(c => c.Id == id));
        }

        public void Update(Commit entity)
        {
            dbContext.Entry(entity).State = EntityState.Modified;
        }
    }
}
