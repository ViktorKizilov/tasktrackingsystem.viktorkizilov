﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTrackingSystem.Data.Entities;
using TaskTrackingSystem.Data.Interfaces;

namespace TaskTrackingSystem.Data.Repositories
{
    public class DepatmentRepository : IDepatmentRepository
    {
        private readonly TaskSystemDBContext dbContext;
        public DepatmentRepository(TaskSystemDBContext dbContext) => this.dbContext = dbContext;

        public async Task AddAsync(Department entity)
        {
            await Task.Run(() => dbContext.Departments.Add(entity));
        }

        public void Delete(Department entity)
        {
            dbContext.Departments.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            Department department = dbContext.Departments.Find(id);
            if (department != null)
                await Task.Run(() => dbContext.Departments.Remove(department));
        }

        public IQueryable<Department> GetAll()
        {
            return dbContext.Departments;
        }

        public IQueryable<Department> GetAllWithDetails()
        {
            return dbContext.Departments.Include(d => d.BossDepartment).Include(d => d.Positions);
        }

        public async Task<Department> GetByIdAsync(int id)
        {
            return await Task.Run(() => dbContext.Departments.FirstOrDefault(d => d.Id == id));
        }

        public async Task<Department> GetByIdWithDetailsAsync(int id)
        {
            return await Task.Run(() => dbContext.Departments
            .Include(d => d.BossDepartment)
            .Include(d => d.Positions)
            .FirstOrDefault(d => d.Id == id));
        }

        public void Update(Department entity)
        {
            dbContext.Entry(entity).State = EntityState.Modified;
        }
    }
}
