﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTrackingSystem.Data.Entities;
using TaskTrackingSystem.Data.Interfaces;

namespace TaskTrackingSystem.Data.Repositories
{
    public class PositionRecordRepository : IPositionRecordRepository
    {
        private readonly TaskSystemDBContext dbContext;
        public PositionRecordRepository(TaskSystemDBContext dbContext) => this.dbContext = dbContext;

        public async Task AddAsync(PositionRecord entity)
        {
            await Task.Run(() => dbContext.PositionRecords.Add(entity));
        }

        public void Delete(PositionRecord entity)
        {
            dbContext.PositionRecords.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            PositionRecord positionRecord = dbContext.PositionRecords.Find(id);
            if (positionRecord != null)
                await Task.Run(() => dbContext.PositionRecords.Remove(positionRecord));
        }

        public IQueryable<PositionRecord> GetAll()
        {
            return dbContext.PositionRecords;
        }

        public IQueryable<PositionRecord> GetAllWithDetails()
        {
            return dbContext.PositionRecords
                        .Include(pr => pr.Position)
                        .Include(pr => pr.Position.Department)
                        .Include(pr => pr.User);
        }

        public async Task<PositionRecord> GetByIdAsync(int id)
        {
            return await Task.Run(() => dbContext.PositionRecords.FirstOrDefault(d => d.Id == id));
        }
        public async Task<PositionRecord> GetByIdWithDetailsAsync(int id)
        {
            return await Task.Run(() => dbContext.PositionRecords
            .Include(pr => pr.Position)
            .Include(pr => pr.Position.Department)
            .Include(pr => pr.User)
            .FirstOrDefaultAsync(pr => pr.Id == id));
        }

        public void Update(PositionRecord entity)
        {
            dbContext.Entry(entity).State = EntityState.Modified;
        }
    }
}
