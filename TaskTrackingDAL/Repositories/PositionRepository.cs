﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTrackingSystem.Data.Entities;
using TaskTrackingSystem.Data.Interfaces;

namespace TaskTrackingSystem.Data.Repositories
{
    public class PositionRepository : IPositionRepository
    {
        private readonly TaskSystemDBContext dbContext;
        public PositionRepository(TaskSystemDBContext dbContext) => this.dbContext = dbContext;

        public async Task AddAsync(Position entity)
        {
            await Task.Run(() => dbContext.Positions.Add(entity));
        }

        public void Delete(Position entity)
        {
            dbContext.Positions.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            Position position = dbContext.Positions.Find(id);
            if (position != null)
                await Task.Run(() => dbContext.Positions.Remove(position));
        }

        public IQueryable<Position> GetAll()
        {
            return dbContext.Positions;
        }
        public IQueryable<Position> GetAllWithDetails()
        {
            return dbContext.Positions
                .Include(p => p.Department)
                .Include(p => p.BossPosition)
                .Include(p => p.PositionRecords);
        }

        public async Task<Position> GetByIdAsync(int id)
        {
            return await Task.Run(() => dbContext.Positions.FirstOrDefault(d => d.Id == id));
        }
        public async Task<Position> GetByIdWithDetailsAsync(int id)
        {
            return await Task.Run(() => dbContext.Positions
            .Include(p => p.Department)
            .Include(p => p.BossPosition)
            .Include(p => p.PositionRecords)
            .FirstOrDefaultAsync(p => p.Id == id));
        }

        public void Update(Position entity)
        {
            dbContext.Entry(entity).State = EntityState.Modified;
        }
    }
}
