﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTrackingSystem.Data.Entities;
using TaskTrackingSystem.Data.Interfaces;

namespace TaskTrackingSystem.Data.Repositories
{
    public class ProjectRepository : IProjectRepository
    {
        private readonly TaskSystemDBContext dbContext;
        public ProjectRepository(TaskSystemDBContext dbContext) => this.dbContext = dbContext;

        public async Task AddAsync(Project entity)
        {
            var creator = dbContext.Users.Where(user => user.Id == entity.CreatorId).AsEnumerable().LastOrDefault();
            var newEntity = entity;

            newEntity.Contributors = new System.Collections.ObjectModel.Collection<User>() { creator };

            await Task.Run(() => dbContext.Projects.Add(newEntity));
        }

        public void Delete(Project entity)
        {
            dbContext.Projects.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            Project project = dbContext.Projects.Find(id);
            if (project != null)
            {
                foreach (var attachent in project.Attachments)
                    await Task.Run(() => dbContext.Attachments.Remove(attachent));

                await Task.Run(() => dbContext.Projects.Remove(project));
            }
        }

        public IQueryable<Project> GetAll()
        {
            return dbContext.Projects;
        }

        public IQueryable<Project> GetAllWithDetails()
        {
            return dbContext.Projects
                .Include(p => p.Assignments)
                .Include(p => p.Attachments)
                .Include(p => p.Contributors)
                .Include(p => p.Creator)
                .Include(p => p.Status);
        }

        public async Task<Project> GetByIdAsync(int id)
        {
            return await Task.Run(() => dbContext.Projects.FirstOrDefaultAsync(a => a.Id == id));
        }

        public async Task<Project> GetByIdWithDetailsAsync(int id)
        {
            return await Task.Run(() => dbContext.Projects
                .Include(p => p.Assignments)
                .Include(p => p.Attachments)
                .Include(p => p.Contributors)
                .Include(p => p.Creator)
                .Include(p => p.Status)
                .FirstOrDefaultAsync(p => p.Id == id));
        }

        public void Update(Project entity)
        {
            dbContext.Entry(entity).State = EntityState.Modified;
        }
    }
}
