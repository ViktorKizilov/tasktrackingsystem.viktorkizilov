﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTrackingSystem.Data.Entities;
using TaskTrackingSystem.Data.Interfaces;

namespace TaskTrackingSystem.Data.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly TaskSystemDBContext dbContext;
        public UserRepository(TaskSystemDBContext dbContext) => this.dbContext = dbContext;

        public async Task AddAsync(User entity)
        {
            await Task.Run(() => dbContext.Users.Add(entity));
        }

        public void Delete(User entity)
        {
            dbContext.Users.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            User user = dbContext.Users.Find(id);
            if (user != null)
                await Task.Run(() => dbContext.Users.Remove(user));
        }

        public IQueryable<User> GetAll()
        {
            return dbContext.Users;
        }

        public IQueryable<User> GetAllWithDetails()
        {
            return dbContext.Users
                 .Include(u => u.ContributedAssignments)
                 .Include(u => u.ContributedProjects)
                 .Include(u => u.CreatedAssignments)
                 .Include(u => u.CreatedProjects)
                 .Include(u => u.PositionsHistory)
                 .Include(u => u.Role);
        }

        public async Task<User> GetByIdAsync(int id)
        {
            return await Task.Run(() => dbContext.Users.FirstOrDefaultAsync(u => u.Id == id));
        }

        public async Task<User> GetByIdWithDetailsAsync(int id)
        {
            return await Task.Run(() => dbContext.Users
                 .Include(u => u.ContributedAssignments)
                 .Include(u => u.ContributedProjects)
                 .Include(u => u.CreatedAssignments)
                 .Include(u => u.CreatedProjects)
                 .Include(u => u.PositionsHistory)
                 .Include(u => u.Role)
                 .FirstOrDefaultAsync(u => u.Id == id));
        }

        public void Update(User entity)
        {
            dbContext.Entry(entity).State = EntityState.Modified;
        }
    }
}
