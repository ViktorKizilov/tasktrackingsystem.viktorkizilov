﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TaskTrackingSystem.Data.Entities;

namespace TaskTrackingSystem.Data
{
    public class TaskSystemDBContext : DbContext
    {
        public TaskSystemDBContext(DbContextOptions<TaskSystemDBContext> options) : base(options)
        {
        }
        public DbSet<Assignment> Assignments { get; set; }
        public DbSet<Attachment> Attachments { get; set; }
        public DbSet<Commit> Commits { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<PositionRecord> PositionRecords { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<ProjectStatus> ProjectStatuses { get; set; }
        public DbSet<AssignmentStatus> AssignmentStatuses { get; set; }
        public DbSet<User> Users { get; set; }

        /*
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Role>().HasData(
                new Role { Id = 1, Name = "Admin" },
                new Role { Id = 2, Name = "User" });

            modelBuilder.Entity<Department>().HasData(
                new Department { Id = 1, Name = "Management" },
                new Department { Id = 2, Name = "Marketing", BossDepartmentId = 1 },
                new Department { Id = 3, Name = "Production", BossDepartmentId = 1 },
                new Department { Id = 4, Name = "R&D", BossDepartmentId = 1 },
                new Department { Id = 5, Name = "Trade Marketing", BossDepartmentId = 2 },
                new Department { Id = 6, Name = "Brand Management", BossDepartmentId = 2 },
                new Department { Id = 7, Name = "Factory management", BossDepartmentId = 3 },
                new Department { Id = 8, Name = "Modern Trade", BossDepartmentId = 5 });

            modelBuilder.Entity<Position>().HasData(
                new Position { Id = 1, Name = "CEO", DepartmentId = 1 },
                new Position { Id = 2, Name = "Marketing Director", DepartmentId = 2, BossPositionId = 1 },
                new Position { Id = 3, Name = "Production Director", DepartmentId = 3, BossPositionId = 1 },
                new Position { Id = 4, Name = "Development Director", DepartmentId = 4, BossPositionId = 1 },
                new Position { Id = 5, Name = "Channel Manager", DepartmentId = 5, BossPositionId = 2 },
                new Position { Id = 6, Name = "Modern Trade Manager", DepartmentId = 8, BossPositionId = 5 },
                new Position { Id = 7, Name = "Shopper Marketing Manager", DepartmentId = 6, BossPositionId = 2 },
                new Position { Id = 8, Name = "Modern Trade Specialist", DepartmentId = 8, BossPositionId = 6 });
            
            modelBuilder.Entity<User>().HasData(
                new User { Id = 1, Name = "Viktor", Surname = "Kizilov", Email = "v.kizilov@tasksystem.com", Password = "Trial!1", BirthDate = new DateTime(1995, 5, 13), RoleId = 1 },
                new User { Id = 2, Name = "Bill", Surname = "Gates", Email = "b.gates@tasksystem.com", Password = "Trial!2", BirthDate = new DateTime(1955, 10, 28), RoleId = 2 },
                new User { Id = 3, Name = "Elon", Surname = "Musk", Email = "e.mask@tasksystem.com", Password = "Trial!3", BirthDate = new DateTime(1971, 6, 28), RoleId = 2 },
                new User { Id = 4, Name = "George", Surname = "Gershwin", Email = "g.gershwin@tasksystem.com", Password = "Trial!4", BirthDate = new DateTime(1991, 6, 20), RoleId = 2 },
                new User { Id = 5, Name = "Oscar", Surname = "Peterson", Email = "o.peterson@tasksystem.com", Password = "Trial!5", BirthDate = new DateTime(1990, 1, 20), RoleId = 2 });
            
            modelBuilder.Entity<PositionRecord>().HasData(
                new PositionRecord { Id = 1, PositionId = 1, UserId = 1, FirstDay = new DateTime(2019, 11, 17) },
                new PositionRecord { Id = 2, PositionId = 2, UserId = 2, FirstDay = new DateTime(2019, 11, 17), LastDay = new DateTime(2019, 12, 17) },
                new PositionRecord { Id = 3, PositionId = 3, UserId = 2, FirstDay = new DateTime(2019, 12, 18) },
                new PositionRecord { Id = 4, PositionId = 2, UserId = 3, FirstDay = new DateTime(2019, 12, 18) },
                new PositionRecord { Id = 5, PositionId = 5, UserId = 4, FirstDay = new DateTime(2019, 11, 17) },
                new PositionRecord { Id = 6, PositionId = 6, UserId = 5, FirstDay = new DateTime(2019, 11, 17) });

            modelBuilder.Entity<ProjectStatus>().HasData(
                new ProjectStatus { Id = 1, Name = "Open" },
                new ProjectStatus { Id = 2, Name = "Revision" },
                new ProjectStatus { Id = 3, Name = "Implementing" },
                new ProjectStatus { Id = 4, Name = "Succeeded" },
                new ProjectStatus { Id = 5, Name = "Failed" });

            modelBuilder.Entity<AssignmentStatus>().HasData(
                new AssignmentStatus { Id = 1, Name = "Working on" },
                new AssignmentStatus { Id = 2, Name = "On revision" },
                new AssignmentStatus { Id = 3, Name = "Returned after revision" },
                new AssignmentStatus { Id = 4, Name = "Submitted" });

            modelBuilder.Entity<Project>().HasData(
                new Project
                {
                    Id = 1,
                    CreatorId = 1,
                    CreationDate = new DateTime(2019, 11, 17),
                    DeadLine = new DateTime(2019, 12, 30),
                    Description = "FirstProject Description",
                    Name = "FirstProject",
                    ProjectStatusId = 1,
                    StatusChangedDate = new DateTime(2019, 11, 17)
                },
                new Project
                {
                    Id = 2,
                    CreatorId = 2,
                    CreationDate = new DateTime(2019, 12, 17),
                    DeadLine = new DateTime(2019, 12, 30),
                    Description = "SecondProject Description",
                    Name = "SecondProject",
                    ProjectStatusId = 4,
                    StatusChangedDate = new DateTime(2020, 5, 12)
                });

            modelBuilder.Entity<Assignment>().HasData(
                new Assignment
                {
                    Id = 1,
                    CreatorId = 1,
                    ExecutorId = 2,
                    Name = "Marketing plan",
                    CreationDate = new DateTime(2019, 11, 17),
                    DeadLine = new DateTime(2019, 12, 17),
                    ProjectId = 1,
                    StatusId = 4,
                    Description = "Create marketing plan for 2020",
                    StatusChangedDate = new DateTime(2019, 12, 18)
                });

            modelBuilder.Entity<Commit>().HasData(
                new Commit
                {
                    Id = 1,
                    AssignmentId = 1,
                    ContributorId = 2,
                    Name = "Marketing plan 2020",
                    Description = "Presentation in PDF format",
                    CreationDate = new DateTime(2019, 12, 10)
                });

            modelBuilder.Entity<Attachment>().HasData(
                new Attachment
                {
                    Id = 1,
                    CommitId = 1,
                    Name = "Marketing plan 2020",
                    Path = @"C:\Users\User999\Desktop\EPAM_courses\Final_task\TaskTracking\WebAPI\AttachedFiles\Marketing plan 2020.pdf"
                });            
        }
        */
    }
}
