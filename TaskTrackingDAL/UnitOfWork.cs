﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TaskTrackingSystem.Data.Interfaces;
using TaskTrackingSystem.Data.Repositories;

namespace TaskTrackingSystem.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly TaskSystemDBContext dBContext;
        private IAttachmentRepository attachmentRepository;
        private IAssignmentRepository assignmentRepository;
        private ICommitRepository commitRepository;
        private IDepatmentRepository depatmentRepository;
        private IPositionRecordRepository positionRecordRepository;
        private IPositionRepository positionRepository;
        private IProjectRepository projectRepository;
        private IUserRepository userRepository;
        public UnitOfWork(DbContextOptions<TaskSystemDBContext> options)
        {
            dBContext = new TaskSystemDBContext(options);
        }

        public IAttachmentRepository AttachmentRepository
        {
            get
            {
                if (attachmentRepository == null)
                    attachmentRepository = new AttachmentRepository(dBContext);
                return attachmentRepository;
            }

        }
        
        public IAssignmentRepository AssignmentRepository
        {
            get
            {
                if (assignmentRepository == null)
                    assignmentRepository = new AssignmentRepository(dBContext);
                return assignmentRepository;
            }
        }

        public ICommitRepository CommitRepository
        {
            get
            {
                if (commitRepository == null)
                    commitRepository = new CommitRepository(dBContext);
                return commitRepository;
            }
        }

        public IDepatmentRepository DepatmentRepository
        {
            get
            {
                if (depatmentRepository == null)
                    depatmentRepository = new DepatmentRepository(dBContext);
                return depatmentRepository;
            }
        }

        public IPositionRecordRepository PositionRecordRepository
        {
            get
            {
                if (positionRecordRepository == null)
                    positionRecordRepository = new PositionRecordRepository(dBContext);
                return positionRecordRepository;
            }
        }

        public IPositionRepository PositionRepository
        {
            get
            {
                if (positionRepository == null)
                    positionRepository = new PositionRepository(dBContext);
                return positionRepository;
            }
        }

        public IProjectRepository ProjectRepository
        {
            get
            {
                if (projectRepository == null)
                    projectRepository = new ProjectRepository(dBContext);
                return projectRepository;
            }
        }

        public IUserRepository UserRepository
        {
            get
            {
                if (userRepository == null)
                    userRepository = new UserRepository(dBContext);
                return userRepository;
            }
        }

        public async Task<int> SaveAsync()
        {
            return await Task.Run(() => dBContext.SaveChangesAsync());
        }
    }
}
