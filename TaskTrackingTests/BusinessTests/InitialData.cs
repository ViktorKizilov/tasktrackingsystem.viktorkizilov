﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using TaskTrackingSystem.Data.Entities;
using TaskTrackingSystem.Business.Models;

namespace TaskTrackingSystem.Tests.BusinessTests
{
    public static class InitialData
    {
        private static IEnumerable<Role> Roles =>
            new List<Role>()
           {
                new Role { Id = 1, Name = "Admin" },
                new Role { Id = 2, Name = "User" }
           };

        private static IEnumerable<Department> Departments =>
             new List<Department>()
            {
            new Department { Id = 1, Name = "Management" },
            new Department { Id = 2, Name = "Marketing", BossDepartmentId = 1 },
            new Department { Id = 3, Name = "Production", BossDepartmentId = 1 },
            new Department { Id = 4, Name = "R&D", BossDepartmentId = 1 },
            new Department { Id = 5, Name = "Trade Marketing", BossDepartmentId = 2 },
            new Department { Id = 6, Name = "Brand Management", BossDepartmentId = 2 },
            new Department { Id = 7, Name = "Factory management", BossDepartmentId = 3 },
            new Department { Id = 8, Name = "Modern Trade", BossDepartmentId = 5 }
            };

        private static IEnumerable<Position> Positions =>
             new List<Position>()
            {
            new Position { Id = 1, Name = "CEO", DepartmentId = 1 },
            new Position { Id = 2, Name = "Marketing Director", DepartmentId = 2, BossPositionId = 1 },
            new Position { Id = 3, Name = "Production Director", DepartmentId = 3, BossPositionId = 1 },
            new Position { Id = 4, Name = "Development Director", DepartmentId = 4, BossPositionId = 1 },
            new Position { Id = 5, Name = "Channel Manager", DepartmentId = 5, BossPositionId = 2 },
            new Position { Id = 6, Name = "Modern Trade Manager", DepartmentId = 8, BossPositionId = 5 },
            new Position { Id = 7, Name = "Shopper Marketing Manager", DepartmentId = 6, BossPositionId = 2 },
            new Position { Id = 8, Name = "Modern Trade Specialist", DepartmentId = 8, BossPositionId = 6 }
            };

        private static IEnumerable<PositionRecord> PositionRecords =>
             new List<PositionRecord>()
            {
            new PositionRecord { Id = 1, PositionId = 1, Position=Positions.ElementAt(0), UserId = 1, FirstDay = new DateTime(2019, 11, 17) },
            new PositionRecord { Id = 2, PositionId = 2, Position=Positions.ElementAt(1), UserId = 2, FirstDay = new DateTime(2019, 11, 17), LastDay = new DateTime(2019, 12, 17) },
            new PositionRecord { Id = 3, PositionId = 3, Position=Positions.ElementAt(2), UserId = 2, FirstDay = new DateTime(2019, 12, 18) },
            new PositionRecord { Id = 4, PositionId = 2, Position=Positions.ElementAt(1), UserId = 3, FirstDay = new DateTime(2019, 12, 18) },
            new PositionRecord { Id = 5, PositionId = 5, Position=Positions.ElementAt(4), UserId = 4, FirstDay = new DateTime(2019, 11, 17) },
            new PositionRecord { Id = 6, PositionId = 6, Position=Positions.ElementAt(5) ,UserId = 5, FirstDay = new DateTime(2019, 11, 17) }
        };

        private static IEnumerable<User> Users =>
     new List<User>()
    {
            new User{Id = 1,Name = "Viktor",Surname = "Kizilov",Email = "v.kizilov@tasksystem.com",Password = "trial",BirthDate = new DateTime(1995, 5, 13),RoleId = 1,
                PositionsHistory =  new Collection<PositionRecord>(){ PositionRecords.ElementAt(0) }},
            new User{Id = 2,Name = "Bill",Surname = "Gates",Email = "b.gates@tasksystem.com",Password = "trial2",BirthDate = new DateTime(1955, 10, 28),RoleId = 2,
                PositionsHistory =  new Collection<PositionRecord>(){ PositionRecords.ElementAt(1), PositionRecords.ElementAt(2) }},
            new User{Id = 3,Name = "Elon",Surname = "Musk",Email = "e.mask@tasksystem.com",Password = "trial3",BirthDate = new DateTime(1971, 6, 28),RoleId = 2},
            new User{Id = 4,Name = "George",Surname = "Gershwin",Email = "g.gershwin@tasksystem.com",Password = "trial4",BirthDate = new DateTime(1991, 6, 20),RoleId = 2},
            new User{Id = 5,Name = "Oscar",Surname = "Peterson",Email = "o.peterson@tasksystem.com",Password = "trial5",BirthDate = new DateTime(1990, 1, 20),RoleId = 2}
    };

        private static IEnumerable<ProjectStatus> ProjectStatuses =>
             new List<ProjectStatus>()
            {
                new ProjectStatus { Id = 1, Name = "Open" },
            new ProjectStatus { Id = 2, Name = "Revision" },
            new ProjectStatus { Id = 3, Name = "Implementing" },
                new ProjectStatus { Id = 4, Name = "Succeeded" },
            new ProjectStatus { Id = 5, Name = "Failed" }
        };

        private static IEnumerable<AssignmentStatus> AssignmentStatuses =>
             new List<AssignmentStatus>()
            {
                new AssignmentStatus { Id = 1, Name = "Working on" },
            new AssignmentStatus { Id = 2, Name = "On revision" },
            new AssignmentStatus { Id = 3, Name = "Returned after revision" },
                new AssignmentStatus { Id = 4, Name = "Submitted" }
        };

        private static IEnumerable<Project> Projects =>
             new List<Project>()
            {
                new Project
            {
                Id = 1,
                CreatorId = 1,
                Creator = Users.ElementAt(0),
                CreationDate = new DateTime(2019, 11, 17),
                DeadLine = new DateTime(2019, 12, 30),
                Description = "FirstProject Description",
                Name = "FirstProject",
                ProjectStatusId = 1,
                Status = ProjectStatuses.ElementAt(0),
                StatusChangedDate = new DateTime(2019, 11, 17),
                Contributors = new Collection<User>(){ Users.ElementAt(0), Users.ElementAt(1)},
                Attachments = new Collection<Attachment>(),
                Assignments = new Collection<Assignment>(){Assignments.ElementAt(0)}
            },
                new Project
            {
                Id = 2,
                CreatorId = 2,
                Creator = Users.ElementAt(1),
                CreationDate = new DateTime(2019, 12, 17),
                DeadLine = new DateTime(2019, 12, 30),
                Description = "SecondProject Description",
                Name = "SecondProject",
                ProjectStatusId = 4,
                Status = ProjectStatuses.ElementAt(3),
                StatusChangedDate = new DateTime(2020, 5, 12),
                Contributors = new Collection<User>(){ Users.ElementAt(1), Users.ElementAt(2), Users.ElementAt(3)},
                Attachments = new Collection<Attachment>(),
                Assignments = new Collection<Assignment>()
            }
            };

        private static IEnumerable<Assignment> Assignments =>
             new List<Assignment>()
            {
                new Assignment
            {
                Id = 1,
                CreatorId = 1,
                Creator = Users.ElementAt(0),
                ExecutorId = 2,
                Executor = Users.ElementAt(1),
                Name = "Marketing plan",
                CreationDate = new DateTime(2019, 11, 17),
                DeadLine = new DateTime(2019, 12, 17),
                ProjectId = 1,
                StatusId = 4,
                Status = AssignmentStatuses.ElementAt(3),
                Description = "Create marketing plan for 2020",
                StatusChangedDate = new DateTime(2019, 12, 18)
            }
            };

        private static IEnumerable<Commit> Commits =>
             new List<Commit>()
            {
                new Commit { Id = 1, AssignmentId = 1, ContributorId = 2, Name = "Marketing plan 2020", Description = "Presentation in PDF format", CreationDate = new DateTime(2019, 12, 10) }
            };

        private static IEnumerable<Attachment> Attachments =>
             new List<Attachment>()
            {
                new Attachment { Id = 1, CommitId = 1, Name = "Marketing plan 2020", Path = @"C:\Users\User999\Desktop\EPAM_courses\Final_task\TaskTracking\TaskTrackingTests\Attachments\Marketing plan 2020.pdf" }
            };

        private static IEnumerable<UserModel> UserModels =>
            new List<UserModel>()
            {
                new UserModel {Id = 1,Name = "Viktor",Surname = "Kizilov",Email = "v.kizilov@tasksystem.com",Password = "trial",BirthDate = new DateTime(1995, 5, 13),RoleId = 1},
                new UserModel {Id = 2,Name = "Bill",Surname = "Gates",Email = "b.gates@tasksystem.com",Password = "trial2",BirthDate = new DateTime(1955, 10, 28),RoleId = 2},
                new UserModel {Id = 3,Name = "Elon",Surname = "Musk",Email = "e.mask@tasksystem.com",Password = "trial3",BirthDate = new DateTime(1971, 6, 28),RoleId = 2},
                new UserModel {Id = 4,Name = "George",Surname = "Gershwin",Email = "g.gershwin@tasksystem.com",Password = "trial4",BirthDate = new DateTime(1991, 6, 20),RoleId = 2 },
                new UserModel {Id = 5,Name = "Oscar",Surname = "Peterson",Email = "o.peterson@tasksystem.com",Password = "trial5",BirthDate = new DateTime(1990, 1, 20),RoleId = 2}
            };

        private static IEnumerable<ProjectModel> ProjectModels =>
            new List<ProjectModel>()
            {
                new ProjectModel
                {
                    Id = 1,
                    CreatorId = 1,
                    CreationDate = new DateTime(2019, 11, 17),
                    DeadLine = new DateTime(2019, 12, 30),
                    Description = "FirstProject Description",
                    Name = "FirstProject",
                    ProjectStatusId = 1,
                    StatusChangedDate = new DateTime(2019, 11, 17),
                    ContributorsIds = new List<int>(){1, 2},
                    AttachmentsIds = new List<int>(),
                    AssignmentsIds = new List<int>(){1}
                },
                    new ProjectModel
                {
                    Id = 2,
                    CreatorId = 2,
                    CreationDate = new DateTime(2019, 12, 17),
                    DeadLine = new DateTime(2019, 12, 30),
                    Description = "SecondProject Description",
                    Name = "SecondProject",
                    ProjectStatusId = 4,
                    StatusChangedDate = new DateTime(2020, 5, 12),
                    ContributorsIds = new List<int>(){2, 3, 4},
                    AttachmentsIds = new List<int>(),
                    AssignmentsIds = new List<int>()
                }
            };

        private static IEnumerable<ProjectFilterModel> ProjectFilterModels =>
            new List<ProjectFilterModel>()
            {
                new ProjectFilterModel {ContributorsIds = new Collection<int>(){2}},
                new ProjectFilterModel {ContributorsIds = new Collection<int>(){1}},
                new ProjectFilterModel {ContributorsIds = new Collection<int>(){2}, CreationDate=new DateTime(2019, 12, 17)},
                new ProjectFilterModel {CreatorId=1},
                new ProjectFilterModel {Name="FirstProject"},
                new ProjectFilterModel {DeadLine=new DateTime(2019, 12, 30)},
                new ProjectFilterModel {CreationDate=new DateTime(2019, 12, 17)}
            };
    }
}
