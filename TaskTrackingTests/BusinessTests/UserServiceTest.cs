﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using TaskTrackingSystem.Data.Entities;
using TaskTrackingSystem.Data.Interfaces;
using TaskTrackingSystem.Business.Interfaces;
using TaskTrackingSystem.Business.Models;
using TaskTrackingSystem.Business.Services;

namespace TaskTrackingSystem.Tests.BusinessTests
{
    public class UserServiceTest
    {
        [Test]
        public void UserService_GetAll_ReturnsUserModels()
        {
            var expected = UserModels.ToList();
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork
                .Setup(m => m.UserRepository.GetAllWithDetails())
                .Returns(Users.AsQueryable);
            IUserService userService = new UserService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            var actual = userService.GetAll().ToList();

            for (int i = 0; i < actual.Count; i++)
            {
                Assert.AreEqual(expected[i].Id, actual[i].Id);
                Assert.AreEqual(expected[i].Name, actual[i].Name);
                Assert.AreEqual(expected[i].Surname, actual[i].Surname);
                Assert.AreEqual(expected[i].Password, actual[i].Password);
                Assert.AreEqual(expected[i].Email, actual[i].Email);
                Assert.AreEqual(expected[i].RoleId, actual[i].RoleId);
            }
        }

        [Test]
        public async Task UserService_AddAsync_AddModel()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.UserRepository.AddAsync(It.IsAny<User>()));
            mockUnitOfWork
                .Setup(m => m.UserRepository.GetAllWithDetails())
                .Returns(Users.AsQueryable);
            mockUnitOfWork.Setup(m => m.PositionRecordRepository.AddAsync(It.IsAny<PositionRecord>()));

            IUserService userService = new UserService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var newUser = new UserModel()
            {
                Name = "Test_Name",
                Surname = "Test_Surname",
                Password = "testpass",
                Email = "t.test_Surname@tasksystem.com",
                BirthDate = new DateTime(1995, 12, 1),
                RoleId = 2,
                NewPositionId = 4
            };

            await userService.AddAsync(newUser);

            mockUnitOfWork.Verify(x => x.UserRepository.AddAsync(
                It.Is<User>(r => r.Name == newUser.Name && r.Surname == newUser.Surname
                && r.Password == newUser.Password && r.Email == newUser.Email
                && r.RoleId == newUser.RoleId)), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }

        private static IEnumerable<Position> Positions =>
            new List<Position>()
            {
                new Position { Id = 1, Name = "CEO", DepartmentId = 1 },
                new Position { Id = 2, Name = "Marketing Director", DepartmentId = 2, BossPositionId = 1 },
                new Position { Id = 3, Name = "Production Director", DepartmentId = 3, BossPositionId = 1 },
                new Position { Id = 4, Name = "Development Director", DepartmentId = 4, BossPositionId = 1 },
                new Position { Id = 5, Name = "Channel Manager", DepartmentId = 5, BossPositionId = 2 },
                new Position { Id = 6, Name = "Modern Trade Manager", DepartmentId = 8, BossPositionId = 5 },
                new Position { Id = 7, Name = "Shopper Marketing Manager", DepartmentId = 6, BossPositionId = 2 },
                new Position { Id = 8, Name = "Modern Trade Specialist", DepartmentId = 8, BossPositionId = 6 }
            };

        private static IEnumerable<PositionRecord> PositionRecords =>
            new List<PositionRecord>()
            {
                new PositionRecord { Id = 1, PositionId = 1, Position=Positions.ElementAt(0), UserId = 1, FirstDay = new DateTime(2019, 11, 17) },
                new PositionRecord { Id = 2, PositionId = 2, Position=Positions.ElementAt(1), UserId = 2, FirstDay = new DateTime(2019, 11, 17), LastDay = new DateTime(2019, 12, 17) },
                new PositionRecord { Id = 3, PositionId = 3, Position=Positions.ElementAt(2), UserId = 2, FirstDay = new DateTime(2019, 12, 18) },
                new PositionRecord { Id = 4, PositionId = 2, Position=Positions.ElementAt(1), UserId = 3, FirstDay = new DateTime(2019, 12, 18) },
                new PositionRecord { Id = 5, PositionId = 5, Position=Positions.ElementAt(4), UserId = 4, FirstDay = new DateTime(2019, 11, 17) },
                new PositionRecord { Id = 6, PositionId = 6, Position=Positions.ElementAt(5) ,UserId = 5, FirstDay = new DateTime(2019, 11, 17) }
            };

        private static IEnumerable<User> Users =>
            new List<User>()
            {
                new User{Id = 1,Name = "Viktor",Surname = "Kizilov",Email = "v.kizilov@tasksystem.com",Password = "trial",BirthDate = new DateTime(1995, 5, 13),RoleId = 1,
                    PositionsHistory =  new Collection<PositionRecord>(){ PositionRecords.ElementAt(0) } },
                new User{Id = 2,Name = "Bill",Surname = "Gates",Email = "b.gates@tasksystem.com",Password = "trial2",BirthDate = new DateTime(1955, 10, 28),RoleId = 2,
                    PositionsHistory =  new Collection<PositionRecord>(){ PositionRecords.ElementAt(1), PositionRecords.ElementAt(2) }},
                new User{Id = 3,Name = "Elon",Surname = "Musk",Email = "e.mask@tasksystem.com",Password = "trial3",BirthDate = new DateTime(1971, 6, 28),RoleId = 2},
                new User{Id = 4,Name = "George",Surname = "Gershwin",Email = "g.gershwin@tasksystem.com",Password = "trial4",BirthDate = new DateTime(1991, 6, 20),RoleId = 2},
                new User{Id = 5,Name = "Oscar",Surname = "Peterson",Email = "o.peterson@tasksystem.com",Password = "trial5",BirthDate = new DateTime(1990, 1, 20),RoleId = 2}
            };

        private static IEnumerable<UserModel> UserModels =>
             new List<UserModel>()
            {
                new UserModel {Id = 1,Name = "Viktor",Surname = "Kizilov",Email = "v.kizilov@tasksystem.com",Password = "trial",BirthDate = new DateTime(1995, 5, 13),RoleId = 1},
                new UserModel {Id = 2,Name = "Bill",Surname = "Gates",Email = "b.gates@tasksystem.com",Password = "trial2",BirthDate = new DateTime(1955, 10, 28),RoleId = 2},
                new UserModel {Id = 3,Name = "Elon",Surname = "Musk",Email = "e.mask@tasksystem.com",Password = "trial3",BirthDate = new DateTime(1971, 6, 28),RoleId = 2},
                new UserModel {Id = 4,Name = "George",Surname = "Gershwin",Email = "g.gershwin@tasksystem.com",Password = "trial4",BirthDate = new DateTime(1991, 6, 20),RoleId = 2 },
                new UserModel {Id = 5,Name = "Oscar",Surname = "Peterson",Email = "o.peterson@tasksystem.com",Password = "trial5",BirthDate = new DateTime(1990, 1, 20),RoleId = 2}
            };
    }
}
