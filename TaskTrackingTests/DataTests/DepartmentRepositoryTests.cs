﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTrackingSystem.Data;
using TaskTrackingSystem.Data.Entities;
using TaskTrackingSystem.Data.Repositories;

namespace TaskTrackingSystem.Tests.DataTests
{
    [TestFixture]
    public class DepartmentRepositoryTests
    {
        [Test]
        public void DepartmentRepository_FindAll_ReturnsAllValues()
        {
            using (var context = new TaskSystemDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var departmentRepository = new DepatmentRepository(context);

                var departments = departmentRepository.GetAll();

                Assert.AreEqual(8, departments.Count());
            }
        }

        [Test]
        public async Task DepartmentRepository_GetById_ReturnsSingleValue()
        {
            using (var context = new TaskSystemDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var departmentRepository = new DepatmentRepository(context);

                var department = await departmentRepository.GetByIdAsync(2);

                Assert.AreEqual(2, department.Id);
                Assert.AreEqual("Marketing", department.Name);
                Assert.AreEqual(1, department.BossDepartmentId);
            }
        }

        [Test]
        public async Task DepartmentRepository_GetById_BossDepartmentNull()
        {
            using (var context = new TaskSystemDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var departmentRepository = new DepatmentRepository(context);

                var department = await departmentRepository.GetByIdAsync(1);

                Assert.AreEqual(1, department.Id);
                Assert.AreEqual("Management", department.Name);
                Assert.AreEqual(null, department.BossDepartmentId);
            }
        }

        [Test]
        public async Task DepartmentRepository_GetByIdWithDetails_ReturnsWithIncludedEntities()
        {
            using (var context = new TaskSystemDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var departmentRepository = new DepatmentRepository(context);

                var department = await departmentRepository.GetByIdWithDetailsAsync(2);

                Assert.AreEqual(2, department.Id);
                Assert.AreEqual("Marketing", department.Name);
                Assert.AreEqual(1, department.BossDepartmentId);
                Assert.AreEqual("Management", department.BossDepartment.Name);
                Assert.AreEqual(1, department.Positions.Count());
                Assert.AreEqual("Marketing Director", department.Positions.FirstOrDefault().Name);
            }
        }

        [Test]
        public void DepartmentRepository_GetAllWithDetails_ReturnsWithIncludedEntities()
        {
            using (var context = new TaskSystemDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var departmentRepository = new DepatmentRepository(context);

                var departments = departmentRepository.GetAllWithDetails();

                Assert.AreEqual(8, departments.Count());
                Assert.AreEqual(1, departments.FirstOrDefault().Positions.Count);
                Assert.AreEqual(3, departments.Where(d => d.BossDepartmentId == 1).Count());
            }
        }

        [Test]
        public async Task DepartmentRepository_DeleteByIdAsync_DeletesEntity()
        {
            using (var context = new TaskSystemDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var departmentRepository = new DepatmentRepository(context);

                await departmentRepository.DeleteByIdAsync(8);
                await context.SaveChangesAsync();

                Assert.AreEqual(7, context.Departments.Count());
            }
        }

        [Test]
        public async Task DepartmentRepository_Delete_DeletesEntity()
        {
            using (var context = new TaskSystemDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var departmentRepository = new DepatmentRepository(context);

                Department department = new Department { Id = 6, Name = "Brand Management", BossDepartmentId = 2 };

                departmentRepository.Delete(department);
                await context.SaveChangesAsync();

                Assert.AreEqual(7, context.Departments.Count());
            }
        }

        [Test]
        public async Task DepartmentRepository_Update_UpdatesEntity()
        {
            using (var context = new TaskSystemDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var departmentRepository = new DepatmentRepository(context);

                Department department = new Department { Id = 6, Name = "Brand Marketing", BossDepartmentId = 2 };

                departmentRepository.Update(department);
                await context.SaveChangesAsync();

                Department updatedDepartment = await departmentRepository.GetByIdAsync(6);

                Assert.AreEqual(6, updatedDepartment.Id);
                Assert.AreEqual("Brand Marketing", updatedDepartment.Name);
                Assert.AreEqual(2, updatedDepartment.BossDepartmentId);
            }
        }

        [Test]
        public async Task DepartmentRepository_AddAsync_AddsValueToDatabase()
        {
            using (var context = new TaskSystemDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var departmentRepository = new DepatmentRepository(context);

                Department department = new Department { Id = 9, Name = "Prototypes Building", BossDepartmentId = 4 };

                await departmentRepository.AddAsync(department);
                await context.SaveChangesAsync();

                Department addedDepartment = context.Departments.Last();

                Assert.AreEqual(9, addedDepartment.Id);
                Assert.AreEqual("Prototypes Building", addedDepartment.Name);
                Assert.AreEqual(4, addedDepartment.BossDepartmentId);
            }
        }
    }
}
