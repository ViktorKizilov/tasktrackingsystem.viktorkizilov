﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTrackingSystem.Data;
using TaskTrackingSystem.Data.Entities;
using TaskTrackingSystem.Data.Repositories;

namespace TaskTrackingSystem.Tests.DataTests
{
    public class PositionRecordRepositoryTests
    {
        [Test]
        public async Task PositionRecordRepository_AddAsync_AddsValueToDatabase()
        {
            using (var context = new TaskSystemDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var positionRecordRepository = new PositionRecordRepository(context);

                PositionRecord record = new PositionRecord { PositionId = 8, UserId = 5, FirstDay = new DateTime(2020, 11, 10) };

                await positionRecordRepository.AddAsync(record);
                await context.SaveChangesAsync();

                PositionRecord addedRecord = context.PositionRecords.Last();

                Assert.AreEqual(7, addedRecord.Id);
                Assert.AreEqual(new DateTime(2020, 11, 10), addedRecord.FirstDay);
                Assert.AreEqual(8, addedRecord.PositionId);
                Assert.AreEqual(5, addedRecord.UserId);
            }
        }

    }
}
