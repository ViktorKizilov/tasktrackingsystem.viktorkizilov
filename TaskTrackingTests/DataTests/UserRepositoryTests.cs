﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTrackingSystem.Data;
using TaskTrackingSystem.Data.Entities;
using TaskTrackingSystem.Data.Repositories;

namespace TaskTrackingSystem.Tests.DataTests
{
    [TestFixture]
    public class UserRepositoryTests
    {
        [Test]
        public void UserRepository_FindAll_ReturnsAllValues()
        {
            using (var context = new TaskSystemDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var userRepository = new UserRepository(context);

                var users = userRepository.GetAll();

                Assert.AreEqual(5, users.Count());
            }
        }

        [Test]
        public async Task UserRepository_GetById_ReturnsSingleValue()
        {
            using (var context = new TaskSystemDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var userRepository = new UserRepository(context);

                var user = await userRepository.GetByIdAsync(2);

                Assert.AreEqual(2, user.Id);
                Assert.AreEqual("Bill", user.Name);
                Assert.AreEqual("Gates", user.Surname);
                Assert.AreEqual("b.gates@tasksystem.com", user.Email);
                Assert.AreEqual("Trial!2", user.Password);
                Assert.AreEqual(new DateTime(1955, 10, 28), user.BirthDate);
                Assert.AreEqual(2, user.RoleId);
            }
        }

        [Test]
        public async Task UserRepository_GetByIdWithDetails_ReturnsWithIncludedEntities()
        {
            using (var context = new TaskSystemDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var userRepository = new UserRepository(context);

                var user = await userRepository.GetByIdWithDetailsAsync(1);

                Assert.AreEqual(1, user.Id);
                Assert.AreEqual("Viktor", user.Name);
                Assert.AreEqual("Kizilov", user.Surname);
                Assert.AreEqual("v.kizilov@tasksystem.com", user.Email);
                Assert.AreEqual("Trial!1", user.Password);
                Assert.AreEqual(new DateTime(1995, 5, 13), user.BirthDate);
                Assert.AreEqual(1, user.RoleId);
                Assert.AreEqual(new DateTime(2019, 11, 17), user.PositionsHistory.FirstOrDefault().FirstDay);
                Assert.AreEqual(1, user.CreatedProjects.FirstOrDefault().CreatorId);
                Assert.AreEqual(1, user.CreatedAssignments.FirstOrDefault().CreatorId);
            }
        }

        [Test]
        public async Task UserRepository_GetByIdWithDetails_ReturnsWithIncludedEntities2()
        {
            using (var context = new TaskSystemDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var userRepository = new UserRepository(context);

                var user = await userRepository.GetByIdWithDetailsAsync(2);

                Assert.AreEqual(2, user.Id);
                Assert.AreEqual("Bill", user.Name);
                Assert.AreEqual("Gates", user.Surname);
                Assert.AreEqual("b.gates@tasksystem.com", user.Email);
                Assert.AreEqual("Trial!2", user.Password);
                Assert.AreEqual(new DateTime(1955, 10, 28), user.BirthDate);
                Assert.AreEqual(2, user.RoleId);
                Assert.AreEqual(2, user.ContributedProjects.Count);
                Assert.AreEqual(1, user.ContributedAssignments.Count);
            }
        }

        [Test]
        public void UserRepository_GetAllWithDetails_ReturnsWithIncludedEntities()
        {
            using (var context = new TaskSystemDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var userRepository = new UserRepository(context);

                var users = userRepository.GetAllWithDetails();

                Assert.AreEqual(5, users.Count());
                Assert.AreEqual(1, users.FirstOrDefault().PositionsHistory.Count);
                Assert.AreEqual(4, users.Where(u => u.Role.Name == "User").Count());
            }
        }

        [Test]
        public async Task UserRepository_DeleteByIdAsync_DeletesEntity()
        {
            using (var context = new TaskSystemDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var userRepository = new UserRepository(context);

                await userRepository.DeleteByIdAsync(5);
                await context.SaveChangesAsync();

                Assert.AreEqual(4, context.Users.Count());
                Assert.AreEqual(4, context.Users.Last().Id);
            }
        }

        [Test]
        public async Task UserRepository_Delete_DeletesEntity()
        {
            using (var context = new TaskSystemDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var userRepository = new UserRepository(context);

                User user = new User { Id = 5, Name = "Oscar", Surname = "Peterson" };

                userRepository.Delete(user);
                await context.SaveChangesAsync();

                Assert.AreEqual(4, context.Users.Count());
            }
        }

        [Test]
        public async Task UserRepository_Update_UpdatesEntity()
        {
            using (var context = new TaskSystemDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var userRepository = new UserRepository(context);

                User user = new User { Id = 4, Surname = "Russel", Email = "g.russel@tasksystem.com" };

                userRepository.Update(user);
                await context.SaveChangesAsync();

                User updatedUser = await userRepository.GetByIdAsync(4);

                Assert.AreEqual(4, updatedUser.Id);
                Assert.AreEqual("Russel", updatedUser.Surname);
                Assert.AreEqual("g.russel@tasksystem.com", updatedUser.Email);
            }
        }

        [Test]
        public async Task UserRepository_AddAsync_AddsValueToDatabase()
        {
            using (var context = new TaskSystemDBContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var userRepository = new UserRepository(context);

                User user = new User 
                {
                    Name = "Draco",
                    Surname = "Malfoy",
                    Email = "d.malfoy@tasksystem.com",
                    Password = "ppotter",
                    BirthDate = new DateTime(1987, 5, 10),
                    RoleId = 2,
                };

                await userRepository.AddAsync(user);
                await context.SaveChangesAsync();

                User addedUser = context.Users.Last();

                Assert.AreEqual(6, addedUser.Id);
                Assert.AreEqual("Draco", addedUser.Name);
                Assert.AreEqual("Malfoy", addedUser.Surname);
                Assert.AreEqual("d.malfoy@tasksystem.com", addedUser.Email);
                Assert.AreEqual("ppotter", addedUser.Password);
                Assert.AreEqual(new DateTime(1987, 5, 10), addedUser.BirthDate);
                Assert.AreEqual(2, addedUser.RoleId);
            }
        }
    }
}
