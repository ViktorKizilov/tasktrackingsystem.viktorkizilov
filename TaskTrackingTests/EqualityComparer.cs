using NUnit.Framework;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using TaskTrackingSystem.Business.Models;
using TaskTrackingSystem.Data.Entities;

namespace TaskTracking.Tests
{
    internal class AssignmentComparer : IEqualityComparer<Assignment>
    {
        public bool Equals([AllowNull] Assignment x, [AllowNull] Assignment y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id
                && x.CreationDate == y.CreationDate
                && x.CreatorId == y.CreatorId
                && x.DeadLine == y.DeadLine
                && x.ExecutorId == y.ExecutorId
                && x.Name == y.Name
                && x.ProjectId == y.ProjectId
                && x.StatusChangedDate == y.StatusChangedDate
                && x.StatusId == y.StatusId;
        }

        public int GetHashCode([DisallowNull] Assignment obj)
        {
            return obj.GetHashCode();
        }
    }

    internal class AttachmentEqualityComparer : IEqualityComparer<Attachment>
    {
        public bool Equals([AllowNull] Attachment x, [AllowNull] Attachment y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id
                && x.AssignmentId == y.AssignmentId
                && x.CommitId == y.CommitId
                && x.Name == y.Name
                && x.Path == y.Path
                && x.ProjectId == y.ProjectId;
        }

        public int GetHashCode([DisallowNull] Attachment obj)
        {
            return obj.GetHashCode();
        }
    }

    internal class CommitEqualityComparer : IEqualityComparer<Commit>
    {
        public bool Equals([AllowNull] Commit x, [AllowNull] Commit y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id
                && x.AssignmentId == y.AssignmentId
                && x.ContributorId == y.ContributorId
                && x.CreationDate == y.CreationDate
                && x.Description == y.Description
                && x.Name == y.Name;
        }

        public int GetHashCode([DisallowNull] Commit obj)
        {
            return obj.GetHashCode();
        }
    }

    internal class DepartmentEqualityComparer : IEqualityComparer<Department>
    {
        public bool Equals([AllowNull] Department x, [AllowNull] Department y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id
                   && x.BossDepartmentId == y.BossDepartmentId
                   && x.Name == y.Name;
        }

        public int GetHashCode([DisallowNull] Department obj)
        {
            return obj.GetHashCode();
        }
    }
    internal class PositionEqualityComparer : IEqualityComparer<Position>
    {
        public bool Equals([AllowNull] Position x, [AllowNull] Position y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id
                   && x.BossPositionId == y.BossPositionId
                   && x.DepartmentId == y.DepartmentId
                   && x.Name == y.Name;
        }

        public int GetHashCode([DisallowNull] Position obj)
        {
            return obj.GetHashCode();
        }
    }
    internal class PositionRecordEqualityComparer : IEqualityComparer<PositionRecord>
    {
        public bool Equals([AllowNull] PositionRecord x, [AllowNull] PositionRecord y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id
                   && x.FirstDay == y.FirstDay
                   && x.LastDay == y.LastDay
                   && x.PositionId == y.PositionId
                   && x.UserId == y.UserId;
        }

        public int GetHashCode([DisallowNull] PositionRecord obj)
        {
            return obj.GetHashCode();
        }
    }
    internal class ProjectEqualityComparer : IEqualityComparer<Project>
    {
        public bool Equals([AllowNull] Project x, [AllowNull] Project y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id
                   && x.CreationDate == y.CreationDate
                   && x.CreatorId == y.CreatorId
                   && x.DeadLine == y.DeadLine
                   && x.Description == y.Description
                   && x.Name == y.Name
                   && x.StatusChangedDate == y.StatusChangedDate
                   && x.ProjectStatusId == y.ProjectStatusId;
        }

        public int GetHashCode([DisallowNull] Project obj)
        {
            return obj.GetHashCode();
        }
    }

    internal class ProjectModelEqualityComparer : IEqualityComparer<ProjectModel>
    {
        public bool Equals([AllowNull] ProjectModel x, [AllowNull] ProjectModel y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id
                   && x.CreationDate == y.CreationDate
                   && x.CreatorId == y.CreatorId
                   && x.DeadLine == y.DeadLine
                   && x.Description == y.Description
                   && x.Name == y.Name
                   && x.StatusChangedDate == y.StatusChangedDate
                   && x.ProjectStatusId == y.ProjectStatusId;
        }

        public int GetHashCode([DisallowNull] ProjectModel obj)
        {
            return obj.GetHashCode();
        }
    }
    internal class RoleEqualityComparer : IEqualityComparer<Role>
    {
        public bool Equals([AllowNull] Role x, [AllowNull] Role y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id
                  && x.Name == y.Name;
        }

        public int GetHashCode([DisallowNull] Role obj)
        {
            return obj.GetHashCode();
        }
    }
    internal class ProjectStatusEqualityComparer : IEqualityComparer<ProjectStatus>
    {
        public bool Equals([AllowNull] ProjectStatus x, [AllowNull] ProjectStatus y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id
                  && x.Name == y.Name;
        }

        public int GetHashCode([DisallowNull] ProjectStatus obj)
        {
            return obj.GetHashCode();
        }
    }
    internal class AssignmentStatusEqualityComparer : IEqualityComparer<AssignmentStatus>
    {
        public bool Equals([AllowNull] AssignmentStatus x, [AllowNull] AssignmentStatus y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id
                  && x.Name == y.Name;
        }

        public int GetHashCode([DisallowNull] AssignmentStatus obj)
        {
            return obj.GetHashCode();
        }
    }
    internal class UserEqualityComparer : IEqualityComparer<User>
    {
        public bool Equals([AllowNull] User x, [AllowNull] User y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id
                   && x.BirthDate == y.BirthDate
                   && x.Email == y.Email
                   && x.Name == y.Name
                   && x.Password == y.Password
                   && x.RoleId == y.RoleId
                   && x.Surname == y.Surname;
        }

        public int GetHashCode([DisallowNull] User obj)
        {
            return obj.GetHashCode();
        }
    }
}