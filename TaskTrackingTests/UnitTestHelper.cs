﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using TaskTrackingSystem.Data;
using TaskTrackingSystem.Data.Entities;
using TaskTrackingSystem.Business;

namespace TaskTrackingSystem.Tests
{
    internal static class UnitTestHelper
    {
        public static DbContextOptions<TaskSystemDBContext> GetUnitTestDbOptions()
        {
            var options = new DbContextOptionsBuilder<TaskSystemDBContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            using (var context = new TaskSystemDBContext(options))
            {
                SeedData(context);
            }
            return options;
        }

        public static void SeedData(TaskSystemDBContext context)
        {
            context.Roles.Add(new Role { Id = 1, Name = "Admin" });
            context.Roles.Add(new Role { Id = 2, Name = "User" });
            context.Departments.Add(new Department { Id = 1, Name = "Management" });
            context.Departments.Add(new Department { Id = 2, Name = "Marketing", BossDepartmentId = 1 });
            context.Departments.Add(new Department { Id = 3, Name = "Production", BossDepartmentId = 1 });
            context.Departments.Add(new Department { Id = 4, Name = "R&D", BossDepartmentId = 1 });
            context.Departments.Add(new Department { Id = 5, Name = "Trade Marketing", BossDepartmentId = 2 });
            context.Departments.Add(new Department { Id = 6, Name = "Brand Management", BossDepartmentId = 2 });
            context.Departments.Add(new Department { Id = 7, Name = "Factory management", BossDepartmentId = 3 });
            context.Departments.Add(new Department { Id = 8, Name = "Modern Trade", BossDepartmentId = 5 });
            context.Positions.Add(new Position { Id = 1, Name = "CEO", DepartmentId = 1 });
            context.Positions.Add(new Position { Id = 2, Name = "Marketing Director", DepartmentId = 2, BossPositionId = 1 });
            context.Positions.Add(new Position { Id = 3, Name = "Production Director", DepartmentId = 3, BossPositionId = 1 });
            context.Positions.Add(new Position { Id = 4, Name = "Development Director", DepartmentId = 4, BossPositionId = 1 });
            context.Positions.Add(new Position { Id = 5, Name = "Channel Manager", DepartmentId = 5, BossPositionId = 2 });
            context.Positions.Add(new Position { Id = 6, Name = "Modern Trade Manager", DepartmentId = 8, BossPositionId = 5 });
            context.Positions.Add(new Position { Id = 7, Name = "Shopper Marketing Manager", DepartmentId = 6, BossPositionId = 2 });
            context.Positions.Add(new Position { Id = 8, Name = "Modern Trade Specialist", DepartmentId = 8, BossPositionId = 6 });
            context.Users.Add(new User
            {
                Id = 1,
                Name = "Viktor",
                Surname = "Kizilov",
                Email = "v.kizilov@tasksystem.com",
                Password = "Trial!1",
                BirthDate = new DateTime(1995, 5, 13),
                RoleId = 1,
            });
            context.Users.Add(new User
            {
                Id = 2,
                Name = "Bill",
                Surname = "Gates",
                Email = "b.gates@tasksystem.com",
                Password = "Trial!2",
                BirthDate = new DateTime(1955, 10, 28),
                RoleId = 2,
            });
            context.Users.Add(new User
            {
                Id = 3,
                Name = "Elon",
                Surname = "Musk",
                Email = "e.mask@tasksystem.com",
                Password = "Trial!3",
                BirthDate = new DateTime(1971, 6, 28),
                RoleId = 2,
            });
            context.Users.Add(new User
            {
                Id = 4,
                Name = "George",
                Surname = "Gershwin",
                Email = "g.gershwin@tasksystem.com",
                Password = "Trial!4",
                BirthDate = new DateTime(1991, 6, 20),
                RoleId = 2,
            });
            context.Users.Add(new User
            {
                Id = 5,
                Name = "Oscar",
                Surname = "Peterson",
                Email = "o.peterson@tasksystem.com",
                Password = "Trial!5",
                BirthDate = new DateTime(1990, 1, 20),
                RoleId = 2,
            });
            context.PositionRecords.Add(new PositionRecord { Id = 1, PositionId = 1, UserId = 1, FirstDay = new DateTime(2019, 11, 17) });
            context.PositionRecords.Add(new PositionRecord { Id = 2, PositionId = 2, UserId = 2, FirstDay = new DateTime(2019, 11, 17), LastDay = new DateTime(2019, 12, 17) });
            context.PositionRecords.Add(new PositionRecord { Id = 3, PositionId = 3, UserId = 2, FirstDay = new DateTime(2019, 12, 18) });
            context.PositionRecords.Add(new PositionRecord { Id = 4, PositionId = 2, UserId = 3, FirstDay = new DateTime(2019, 12, 18) });
            context.PositionRecords.Add(new PositionRecord { Id = 5, PositionId = 5, UserId = 4, FirstDay = new DateTime(2019, 11, 17) });
            context.PositionRecords.Add(new PositionRecord { Id = 6, PositionId = 6, UserId = 5, FirstDay = new DateTime(2019, 11, 17) });
            context.ProjectStatuses.Add(new ProjectStatus { Id = 1, Name = "Open" });
            context.ProjectStatuses.Add(new ProjectStatus { Id = 2, Name = "Revision" });
            context.ProjectStatuses.Add(new ProjectStatus { Id = 3, Name = "Implementing" });
            context.ProjectStatuses.Add(new ProjectStatus { Id = 4, Name = "Succeeded" });
            context.ProjectStatuses.Add(new ProjectStatus { Id = 5, Name = "Failed" });
            context.AssignmentStatuses.Add(new AssignmentStatus { Id = 1, Name = "Working on" });
            context.AssignmentStatuses.Add(new AssignmentStatus { Id = 2, Name = "On revision" });
            context.AssignmentStatuses.Add(new AssignmentStatus { Id = 3, Name = "Returned after revision" });
            context.AssignmentStatuses.Add(new AssignmentStatus { Id = 4, Name = "Submitted" });
            context.Projects.Add(new Project
            {
                Id = 1,
                CreatorId = 1,
                CreationDate = new DateTime(2019, 11, 17),
                DeadLine = new DateTime(2019, 12, 30),
                Description = "FirstProject Description",
                Name = "FirstProject",
                ProjectStatusId = 1,
                StatusChangedDate = new DateTime(2019, 11, 17),
                Contributors = new List<User>()
            });
            context.Projects.Add(new Project
            {
                Id = 2,
                CreatorId = 2,
                CreationDate = new DateTime(2019, 12, 17),
                DeadLine = new DateTime(2019, 12, 30),
                Description = "SecondProject Description",
                Name = "SecondProject",
                ProjectStatusId = 4,
                StatusChangedDate = new DateTime(2020, 5, 12),
                Contributors = new List<User>()
            });
            context.Assignments.Add(new Assignment
            {
                Id = 1,
                CreatorId = 1,
                ExecutorId = 2,
                Name = "Marketing plan",
                CreationDate = new DateTime(2019, 11, 17),
                DeadLine = new DateTime(2019, 12, 17),
                ProjectId = 1,
                StatusId = 4,
                Description = "Create marketing plan for 2020",
                StatusChangedDate = new DateTime(2019, 12, 18)
            });
            context.Commits.Add(new Commit { Id = 1, AssignmentId = 1, ContributorId = 2, Name = "Marketing plan 2020", Description = "Presentation in PDF format", CreationDate = new DateTime(2019, 12, 10) });
            context.Attachments.Add(new Attachment { Id = 1, ProjectId = 1, Name = "Marketing plan 2020", Path = @"C:\Users\User999\Desktop\EPAM_courses\Final_task\TaskTracking\TaskTrackingTests\Attachments\Marketing plan 2020.pdf" });
            context.SaveChanges();

            AddOrUpdateContributor(context, 1, 1);
            AddOrUpdateContributor(context, 1, 2);
            AddOrUpdateContributor(context, 2, 2);
            AddOrUpdateContributor(context, 2, 3);
            AddOrUpdateContributor(context, 2, 4);
            context.SaveChanges();
        }

        public static void AddOrUpdateContributor(TaskSystemDBContext context, int projectId, int contributorId)
        {
            var project = context.Projects.SingleOrDefault(p => p.Id == projectId);
            var contributor = project.Contributors.SingleOrDefault(c => c.Id == contributorId);
            if (contributor == null)
                project.Contributors.Add(context.Users.Single(u => u.Id == contributorId));
        }

        public static Mapper CreateMapperProfile()
        {
            var myProfile = new AutomapperProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));

            return new Mapper(configuration);
        }

    }
}
