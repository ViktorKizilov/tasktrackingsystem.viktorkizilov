﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TaskTrackingSystem.Business.Interfaces;
using TaskTrackingSystem.Business.Models;
using TaskTrackingSystem.Business.Validation;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AssignmentsController : ControllerBase
    {
        IAssignmentService AssignmentService { get; set; }
        private readonly IWebHostEnvironment env;
        public AssignmentsController(IAssignmentService assignmentService, IWebHostEnvironment hostEnvironment)
        {
            AssignmentService = assignmentService;
            env = hostEnvironment;
        }

        [HttpGet]
        public ActionResult<IEnumerable<AssignmentModel>> GetByFilter([FromQuery] AssignmentFilterModel model)
        {
            IEnumerable<AssignmentModel> assignments = AssignmentService.GetByFilter(model);
            if (!assignments.Any())
                return AssignmentService.GetAll().ToList();
            else
                return assignments.ToList();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<AssignmentModel>>> GetById(int id)
        {
            var assignment = await AssignmentService.GetByIdAsync(id);
            if (assignment == null)
                return NotFound();
            return new ObjectResult(assignment);
        }

        [HttpPost]
        public async Task<ActionResult> Add([FromBody] AssignmentModel AssignmentModel)
        {
            try
            {
                await AssignmentService.AddAsync(AssignmentModel);
            }
            catch (IncorrectInputException)
            {
                return BadRequest();
            }
            AssignmentModel assignment = AssignmentService.GetAll().LastOrDefault();
            return new CreatedResult($"http://localhost/api/assignments/{assignment.Id}", assignment);
        }

        [HttpPut]
        public async Task<ActionResult> Update(AssignmentModel AssignmentModel)
        {
            try
            {
                await AssignmentService.UpdateAsync(AssignmentModel);
            }
            catch (IncorrectInputException)
            {
                return BadRequest();
            }
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var assignment = await AssignmentService.GetByIdAsync(id);
            if (assignment == null)
                return NotFound();
            await AssignmentService.DeleteByIdAsync(id);
            return Ok();
        }

        [HttpPost]
        [Route("{id}/attachments")]
        public async Task<ActionResult> AddFiles([FromBody] IFormFileCollection attachments, int id)
        {
            string path = Path.Combine(env.ContentRootPath, "/AttachedFiles");
            await AssignmentService.AddAttachmentsAsync(attachments, id, path);

            return new CreatedResult($"http://localhost/api/assignments/{id}/attachments", AssignmentService.GetAttachmentsByContainerId(id));
        }

        [HttpDelete]
        [Route("{id}/attachments/{attachid}")]
        public async Task<ActionResult> DeleteFile(int id, int attachid)
        {
            if (await AssignmentService.GetByIdAsync(id) == null)
                return NotFound();

            var attachment = await AssignmentService.GetAttachmentById(attachid);
            if (attachment == null)
                return NotFound();

            await AssignmentService.DeleteAttachmentAsync(attachid);
            return Ok();
        }
    }
}
