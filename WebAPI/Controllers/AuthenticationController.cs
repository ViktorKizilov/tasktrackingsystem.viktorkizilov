﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TaskTrackingSystem.Business.Interfaces;
using TaskTrackingSystem.Business.Models;
using TaskTrackingSystem.Business.Validation;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        IAuthenticationService AuthenticationService { get; set; }
        public AuthenticationController(IAuthenticationService authenticationService)
        {
            AuthenticationService = authenticationService;
        }

        [AllowAnonymous]
        [HttpPost]
        public UserLoggedModel Login(UserAuthModel userAuthModel)
        {
            try
            {
                return AuthenticationService.Authenticate(userAuthModel);
            }
            catch (IncorrectInputException e)
            {
                throw new ApplicationException(e.Message);
            }
        }
    }
}
