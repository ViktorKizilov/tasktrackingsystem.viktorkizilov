﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TaskTrackingSystem.Business.Interfaces;
using TaskTrackingSystem.Business.Models;
using TaskTrackingSystem.Business.Validation;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommitsController : ControllerBase
    {
        ICommitService CommitService { get; set; }
        private readonly IWebHostEnvironment env;
        public CommitsController(ICommitService commitService, IWebHostEnvironment hostEnvironment)
        {
            CommitService = commitService;
            env = hostEnvironment;
        }

        public CommitsController(ICommitService commitService) => CommitService = commitService;

        [HttpGet]
        public ActionResult<IEnumerable<CommitModel>> GetByFilter([FromQuery] CommitFilterModel model)
        {
            IEnumerable<CommitModel> commits = CommitService.GetByFilter(model);
            if (!commits.Any())
                return CommitService.GetAll().ToList();
            else
                return commits.ToList();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<CommitModel>>> GetById(int id)
        {
            var commit = await CommitService.GetByIdAsync(id);
            if (commit == null)
                return NotFound();
            return new ObjectResult(commit);
        }

        [HttpPost]
        public async Task<ActionResult> Add([FromBody] CommitModel CommitModel)
        {
            try
            {
                await CommitService.AddAsync(CommitModel);
            }
            catch (IncorrectInputException)
            {
                return BadRequest();
            }
            CommitModel commit = CommitService.GetAll().LastOrDefault();
            return new CreatedResult($"http://localhost/api/commits/{commit.Id}", commit);
        }

        [HttpPut]
        public async Task<ActionResult> Update(CommitModel CommitModel)
        {
            try
            {
                await CommitService.UpdateAsync(CommitModel);
            }
            catch (IncorrectInputException)
            {
                return BadRequest();
            }
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var commit = await CommitService.GetByIdAsync(id);
            if (commit == null)
                return NotFound();
            await CommitService.DeleteByIdAsync(id);
            return Ok();
        }

        [HttpPost]
        [Route("{id}/attachments")]
        public async Task<ActionResult> AddFiles([FromBody] IFormFileCollection attachments, int id)
        {
            string path = Path.Combine(env.ContentRootPath, "/AttachedFiles");
            await CommitService.AddAttachmentsAsync(attachments, id, path);

            return new CreatedResult($"http://localhost/api/commits/{id}/attachments", CommitService.GetAttachmentsByContainerId(id));
        }

        [HttpDelete]
        [Route("{id}/attachments/{attachid}")]
        public async Task<ActionResult> DeleteFile(int id, int attachid)
        {
            if (await CommitService.GetByIdAsync(id) == null)
                return NotFound();

            var attachment = await CommitService.GetAttachmentById(attachid);
            if (attachment == null)
                return NotFound();

            await CommitService.DeleteAttachmentAsync(attachid);
            return Ok();
        }
    }
}
