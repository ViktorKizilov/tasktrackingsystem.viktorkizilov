﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TaskTrackingSystem.Business.Interfaces;
using TaskTrackingSystem.Business.Models;
using TaskTrackingSystem.Business.Validation;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentsController : ControllerBase
    {
        IDepartmentService DepartmentService { get; set; }
        public DepartmentsController(IDepartmentService departmentService) => DepartmentService = departmentService;

        [HttpGet]
        public async Task<ActionResult<IEnumerable<DepartmentModel>>> GetAll()
        {
            return await Task.Run(() => DepartmentService.GetAll().ToList());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<DepartmentModel>>> GetById(int id)
        {
            var department = await DepartmentService.GetByIdAsync(id);
            if (department == null)
                return NotFound();
            return new ObjectResult(department);
        }

        [HttpPost]
        public async Task<ActionResult> Add([FromBody] DepartmentModel DepartmentModel)
        {
            try
            {
                await DepartmentService.AddAsync(DepartmentModel);
            }
            catch (IncorrectInputException)
            {
                return BadRequest();
            }
            DepartmentModel department = DepartmentService.GetAll().LastOrDefault();
            return new CreatedResult($"http://localhost/api/departments/{department.Id}", department);
        }

        [HttpPut]
        public async Task<ActionResult> Update(DepartmentModel DepartmentModel)
        {
            try
            {
                await DepartmentService.UpdateAsync(DepartmentModel);
            }
            catch (IncorrectInputException)
            {
                return BadRequest();
            }
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var department = await DepartmentService.GetByIdAsync(id);
            if (department == null)
                return NotFound();
            await DepartmentService.DeleteByIdAsync(id);
            return Ok();
        }
    }
}
