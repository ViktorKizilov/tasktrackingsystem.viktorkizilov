﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TaskTrackingSystem.Business.Interfaces;
using TaskTrackingSystem.Business.Models;
using TaskTrackingSystem.Business.Validation;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "Admin")]
    [ApiController]
    public class PositionsController : ControllerBase
    {
        IPositionService PositionService { get; set; }
        public PositionsController(IPositionService positionService) => PositionService = positionService;

        [HttpGet]
        public async Task<ActionResult<IEnumerable<PositionModel>>> GetAll()
        {
            return await Task.Run(() => PositionService.GetAll().ToList());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<PositionModel>>> GetById(int id)
        {
            var position = await PositionService.GetByIdAsync(id);
            if (position == null)
                return NotFound();
            return new ObjectResult(position);
        }

        [HttpPost]
        public async Task<ActionResult> Add([FromBody] PositionModel PositionModel)
        {
            try
            {
                await PositionService.AddAsync(PositionModel);
            }
            catch (IncorrectInputException)
            {
                return BadRequest();
            }
            PositionModel position = PositionService.GetAll().LastOrDefault();
            return new CreatedResult($"http://localhost/api/positions/{position.Id}", position);
        }

        [HttpPut]
        public async Task<ActionResult> Update(PositionModel PositionModel)
        {
            try
            {
                await PositionService.UpdateAsync(PositionModel);
            }
            catch (IncorrectInputException)
            {
                return BadRequest();
            }
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var position = await PositionService.GetByIdAsync(id);
            if (position == null)
                return NotFound();
            await PositionService.DeleteByIdAsync(id);
            return Ok();
        }
    }
}
