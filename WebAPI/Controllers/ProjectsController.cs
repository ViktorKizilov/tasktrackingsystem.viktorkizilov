﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TaskTrackingSystem.Business.Interfaces;
using TaskTrackingSystem.Business.Models;
using TaskTrackingSystem.Business.Validation;
using TaskTrackingSystem.Data.Entities;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        IProjectService ProjectService { get; set; }
        private readonly IWebHostEnvironment env;
        public ProjectsController(IProjectService projectService, IWebHostEnvironment hostEnvironment)
        {
            ProjectService = projectService;
            env = hostEnvironment;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ProjectModel>> GetByFilter([FromQuery] ProjectFilterModel model)
        {
            IEnumerable<ProjectModel> projects = ProjectService.GetByFilter(model);
            if (!projects.Any())
                return ProjectService.GetAll().ToList();
            else
                return projects.ToList();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<ProjectModel>>> GetById(int id)
        {
            var project = await ProjectService.GetByIdAsync(id);
            if (project == null)
                return NotFound();
            return new ObjectResult(project);
        }

        [HttpPost]
        public async Task<ActionResult> Add([FromBody] ProjectModel ProjectModel)
        {
            try
            {
                await ProjectService.AddAsync(ProjectModel);
            }
            catch (IncorrectInputException)
            {
                return BadRequest();
            }
            ProjectModel project = ProjectService.GetAll().LastOrDefault();
            return new CreatedResult($"http://localhost/api/projects/{project.Id}", project);
        }

        [HttpPut]
        public async Task<ActionResult> Update(ProjectModel ProjectModel)
        {
            try
            {
                await ProjectService.UpdateAsync(ProjectModel);
            }
            catch (IncorrectInputException)
            {
                return BadRequest();
            }
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var project = await ProjectService.GetByIdAsync(id);
            if (project == null)
                return NotFound();
            await ProjectService.DeleteByIdAsync(id);
            return Ok();
        }

        [HttpGet]
        [Route("{id}/attachments")]
        public ActionResult<IEnumerable<Attachment>> GetFiles(int id)
        {
            return ProjectService.GetAttachmentsByContainerId(id).ToList();
        }

        [HttpPost]
        [Route("{id}/attachments")]
        public async Task<ActionResult> AddFiles([FromBody] IFormFileCollection attachments, int id)
        {
            string path = Path.Combine(env.ContentRootPath, "/AttachedFiles");
            await ProjectService.AddAttachmentsAsync(attachments, id, path);

            return new CreatedResult($"http://localhost/api/projects/{id}/attachments", ProjectService.GetAttachmentsByContainerId(id));
        }

        [HttpDelete]
        [Route("{id}/attachments/{attachid}")]
        public async Task<ActionResult> DeleteFile(int id, int attachid)
        {
            if (await ProjectService.GetByIdAsync(id) == null)
                return NotFound();

            var attachment = await ProjectService.GetAttachmentById(attachid);
            if (attachment == null)
                return NotFound();

            await ProjectService.DeleteAttachmentAsync(attachid);
            return Ok();
        }
    }
}
