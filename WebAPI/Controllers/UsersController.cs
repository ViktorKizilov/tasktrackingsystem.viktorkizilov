﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TaskTrackingSystem.Business.Interfaces;
using TaskTrackingSystem.Business.Models;
using TaskTrackingSystem.Business.Validation;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        IUserService UserService { get; set; }
        public UsersController(IUserService userService) => UserService = userService;

        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserModel>>> GetAll()
        {
            return await Task.Run(() => UserService.GetAll().ToList());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<UserModel>>> GetById(int id)
        {
            var user = await UserService.GetByIdAsync(id);
            if (user == null)
                return NotFound();
            return new ObjectResult(user);
        }

        [HttpPost]
        public async Task<ActionResult> Add([FromBody] UserModel UserModel)
        {
            try
            {
                await UserService.AddAsync(UserModel);
            }
            catch (IncorrectInputException)
            {
                return BadRequest();
            }
            UserModel user = UserService.GetAll().LastOrDefault();
            return new CreatedResult($"http://localhost/api/users/{user.Id}", user);
        }

        [HttpPut]
        public async Task<ActionResult> Update(UserModel UserModel)
        {
            try
            {
                await UserService.UpdateAsync(UserModel);
            }
            catch (IncorrectInputException)
            {
                return BadRequest();
            }
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var user = await UserService.GetByIdAsync(id);
            if (user == null)
                return NotFound();
            await UserService.DeleteByIdAsync(id);
            return Ok();
        }
    }
}
